/***************************************************************************//**
    \file ConfigParser.cpp
    \brief Config file parser interface.
*//****************************************************************************/

#include "ConfigParser.hpp"

namespace Config {

void exitWithError(const std::string &error) {
    std::cout << error;
}

template <>
std::string Convert::string_to_T(std::string const &val) {
    return val;
}

}
