/***************************************************************************//**
    \file Memory.hpp
    \brief The header for the memory interface.
*//****************************************************************************/
#ifndef __MEMORY_H__
#define __MEMORY_H__

#include "Util.hpp"

namespace Mem {

class Cache;

/** The status of the cache after a memory transaction. */
typedef enum CacheStatus {
    CacheStatus_Hit,
    CacheStatus_Miss,
    CacheStatus_None,
} CacheStatus_t;

/** A cache association block. */
typedef struct CacheAssocBlock
{
    Arch::UnsignedWord_t tag; /** The block tag. */
    bool valid;               /** Is the data current. */
    /** The number of times this block has
        not been used in this association. */
    uint64_t notUsedTimes;
    /* Because this is a simulator we dont actually store the data in the cache
       blocks instead we simple report if the data would be there and still get
       the data from main memory. */
} CacheAssocBlock_t;

/** The mode in which to use memory. */
typedef enum Mode {
    Mode_Read,
    Mode_Write,
    Mode_Pass,
    Mode_Special,
} Mode_t;

/** The result of a memory transaction. */
typedef struct Transaction {
    Arch::Address_t  address;
    Arch::Word_t     data;
    CacheStatus_t    status;
} Transaction_t;

/***************************************************************************//**
    Memory class. All interaction with the memory class is done using byte
    aligned addresses, but internally the memory is word aligned.
*//****************************************************************************/
class Memory {
public:
    Memory(uint64_t size, Arch::Address_t textAddressStart, Arch::Address_t stackAddressStart,
           uint64_t cacheSize, uint64_t cacheAssocitivity, uint64_t cacheNumBlocks);
    static Arch::Address_t byte2wordAligned(Arch::Address_t address);
    static Arch::Address_t word2byteAligned(Arch::Address_t address);
    Transaction_t read(Arch::Address_t address);
    Transaction_t write(Arch::Address_t address, Arch::Word_t data);
    Transaction_t readWithCache(Arch::Address_t address);
    Transaction_t writeWithCache(Arch::Address_t address, Arch::Word_t data);
    Transaction_t readWordAligned(Arch::Address_t address);
    Transaction_t writeWordAligned(Arch::Address_t address, Arch::Word_t data);
    void loadTextAndData(FILE* inputFile);
    bool textAddress(Arch::Address_t address);
    bool dataAddress(Arch::Address_t address);
    bool stackAddress(Arch::Address_t address);
    static std::string getModeStr(Mode_t mode);
    Arch::Address_t getTextAddressStart() {return word2byteAligned(textAddressStart); };
    Arch::Address_t getTextAddressEnd() {return word2byteAligned(textAddressEnd); };
    Arch::Address_t getDataAddressStart() {return word2byteAligned(dataAddressStart); };
    Arch::Address_t getDataAddressEnd() {return word2byteAligned(dataAddressEnd); };
    Arch::Address_t getStackAddressStart() {return word2byteAligned(stackAddressStart); };
    Arch::Address_t getStackAddressEnd() {return word2byteAligned(stackAddressEnd); };
    Arch::Address_t getStackAddressMin() {return word2byteAligned(stackAddressMin); };
private:
    const uint64_t size; /** The size in words of the memory. */
    /** The starting address of the text segment. Grows updward. */
    const Arch::Address_t textAddressStart;
    /** The ending address of the text segment. */
    Arch::Address_t textAddressEnd;
    /** The starting address of the  data segment (textAddressEnd + 1). Grows upward. */
    Arch::Address_t dataAddressStart;
    /** The ending address of the  data segment. */
    Arch::Address_t dataAddressEnd;
    /** The starting address of the stack. Grows downward. */
    const Arch::Address_t stackAddressStart;
    /** The ending address of the stack (dataAddressEnd + 1). */
    Arch::Address_t stackAddressEnd;
    /** The minimum valued memory address into the stack.
        Used to calculate the amount of stack space used. */
    Arch::Address_t stackAddressMin;
    /** The stored memory values. */
    std::vector<Arch::Word_t> memory;
    Cache* cache;
};

/***************************************************************************//**
    Cache class. Uses word aligned addressing.

    Below is a cache with size 64, associtivity 2, and spatial locality 4.
    The index is used to select a set and the offset is used to select a block.
    All association blocks are checked for the correct tag data. v = valid bit
                  AssocitivityBlock0                AssocitivitiyBlock1
    set 0 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
    set 1 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
    set 2 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
    set 3 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
    set 4 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
    set 5 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
    set 6 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
    set 7 | v tag [data0 data1 data2 data3] | v tag [data0 data1 data2 data3] |
*//****************************************************************************/
class Cache {
public:
    Cache(uint64_t size, uint64_t associtivity, uint64_t numBlocks);
    CacheStatus_t read(Arch::Address_t address);
    CacheStatus_t write(Arch::Address_t address);
    static std::string getStatusStr(CacheStatus_t status);
    uint64_t getSize(void) { return size; };
    uint64_t getAssocitivity(void) { return associtivity; };
    uint64_t getNumBlocks(void) { return numBlocks; };
    uint64_t getNumSets(void) { return numSets; };
    uint64_t getNumIndexBits(void) { return numIndexBits; };
    uint64_t getNumOffsetBits(void) { return numOffsetBits; };
    uint64_t getNumTagBits(void) { return numTagBits; };
private:
    Arch::Address_t getOffset(Arch::Address_t address);
    Arch::Address_t getIndex(Arch::Address_t address);
    Arch::Address_t getTag(Arch::Address_t address);
    Arch::Address_t replacementPolicy(Arch::Address_t index);
    uint64_t size;          /** The size of the cache must be a power of two. */
    uint64_t associtivity;  /** The associtivity of the cache must be a power of two. */
    uint64_t numBlocks;     /** The number of blocks in each association must be power of two. */
    uint64_t numSets;       /** The number of sets in the cache. */
    uint64_t numIndexBits;  /** The number of index bits of the cache. */
    uint64_t numOffsetBits; /** The number of offset bits of the cache. */
    uint64_t numTagBits;    /** The number of tag bits of the cache. */
    /** The data memory cache. */
    std::vector<std::vector<CacheAssocBlock_t>> cache;
};

extern Transaction_t defaultTransaction;

}

#endif /* __MEMORY_H__ */
