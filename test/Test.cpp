#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "../src/Architecture.h"
#include "../src/Datapath.hpp"
#include "../src/Datapath.cpp"
#include "../src/BranchPredictor.hpp"
#include "../src/BranchPredictor.cpp"
#include "../src/BaseInstruction.hpp"
#include "../src/BaseInstruction.cpp"
#include "../src/Instructions.hpp"
#include "../src/Instructions.cpp"
#include "../src/SpecialInstructions.hpp"
#include "../src/SpecialInstructions.cpp"
#include "../src/ExecutionUnit.hpp"
#include "../src/ExecutionUnit.cpp"
#include "../src/Memory.hpp"
#include "../src/Memory.cpp"
#include "../src/RegisterFile.hpp"
#include "../src/RegisterFile.cpp"
#include "../src/Util.hpp"
#include "../src/Util.cpp"

SCENARIO("Running a program in the datapath.", "[datapath]") {
    GIVEN( "a datapath" ) {
        Datapath::Datapath d;
        FILE* inputFile;
        WHEN( "test r arithmetic and logic" ) {
            inputFile = fopen("samples/test_r_arithmetic_and_logic.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_s0).s == 14 );
                REQUIRE( d.iReg->read(Arch::Reg_s1).s == -34 );
                REQUIRE( d.iReg->read(Arch::Reg_s2).s == 527 );
                REQUIRE( d.iReg->read(Arch::Reg_s3).s == -10 );
                REQUIRE( d.iReg->read(Arch::Reg_s4).s == 1 );
                REQUIRE( d.iReg->read(Arch::Reg_s5).s == -1 );
                REQUIRE( d.iReg->read(Arch::Reg_s6).s == -8 );
                REQUIRE( d.iReg->read(Arch::Reg_s7).s == -96 );
                REQUIRE( d.iReg->read(Arch::Reg_t5).s == 7 );
                REQUIRE( d.iReg->read(Arch::Reg_t6).s == 0 );
                REQUIRE( d.iReg->read(Arch::Reg_t7).s == 1 );
            }
        }
        WHEN( "test i arithmetic and logic" ) {
            inputFile = fopen("samples/test_i_arithmetic_and_logic.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_s0).s == -16 );
                REQUIRE( d.iReg->read(Arch::Reg_s1).s == 92 );
                REQUIRE( d.iReg->read(Arch::Reg_s2).s == 633 );
                REQUIRE( d.iReg->read(Arch::Reg_s3).s == 1 );
                REQUIRE( d.iReg->read(Arch::Reg_s4).s == 9 );
                REQUIRE( d.iReg->read(Arch::Reg_s5).s == -383 );
                REQUIRE( d.iReg->read(Arch::Reg_s6).s == -9 );
                REQUIRE( d.iReg->read(Arch::Reg_s7).s == 1 );
                REQUIRE( d.iReg->read(Arch::Reg_t6).s == -143780567);
            }
        }
        WHEN( "test data hazard" ) {
            inputFile = fopen("samples/test_data_hazard.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_t0).s == -5 );
                REQUIRE( d.iReg->read(Arch::Reg_t1).s == -10 );
                REQUIRE( d.iReg->read(Arch::Reg_t2).s == -15 );
            }
        }
        WHEN( "test fr arithmetic" ) {
            inputFile = fopen("samples/test_fr_arithmetic.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.fReg->read(Arch::Reg_fs0).f == Approx(70.59).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs1).f == Approx(132.93).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs2).f == Approx(-3171.8592).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs3).f == Approx(-3.264677575).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs4).f == Approx(0).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs5).f == Approx(1).epsilon(0.00001));
            }
        }
        WHEN( "test memory" ) {
            inputFile = fopen("samples/test_memory.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_s0).s == 100 );
                REQUIRE( d.iReg->read(Arch::Reg_s1).s == 205 );
                REQUIRE( d.iReg->read(Arch::Reg_s2).s == 106 );
                REQUIRE( d.iReg->read(Arch::Reg_s3).s == 0x56ab );
                REQUIRE( d.iReg->read(Arch::Reg_s4).s == -14 );
                REQUIRE( d.iReg->read(Arch::Reg_s5).s == -32768 );
                REQUIRE( d.iReg->read(Arch::Reg_s6).s == 31 );
                REQUIRE( d.iReg->read(Arch::Reg_s7).s == 0 );
                REQUIRE( d.fReg->read(Arch::Reg_fs0).f == Approx(517.894).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs1).f == Approx(0.01).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs2).f == Approx(-917.4367).epsilon(0.00001));
            }
        }
        WHEN( "test branch and jump" ) {
            inputFile = fopen("samples/test_branch_and_jump.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_t0).s == 11 );
                REQUIRE( d.iReg->read(Arch::Reg_t1).s == 11 );
                REQUIRE( d.fReg->read(Arch::Reg_ft1).f == Approx(0.7).epsilon(0.001));
                REQUIRE( d.fReg->read(Arch::Reg_ft2).f == Approx(0.7).epsilon(0.001));
                REQUIRE( d.iReg->read(Arch::Reg_t2).s == 4 );
                REQUIRE( d.iReg->read(Arch::Reg_ra).s == 116 );
                REQUIRE( d.iReg->read(Arch::Reg_t3).s == 4 );
                REQUIRE( d.iReg->read(Arch::Reg_t4).s == 42 );
            }
        }
        WHEN( "test data segment" ) {
            inputFile = fopen("samples/test_data_segment.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_s0).s == 10 );
                REQUIRE( d.iReg->read(Arch::Reg_s1).s == -17 );
                REQUIRE( d.iReg->read(Arch::Reg_s2).s == -137 );
                REQUIRE( d.iReg->read(Arch::Reg_s3).s == 17 );
            }
        }
        WHEN( "test special instructions" ) {
            inputFile = fopen("samples/test_spu.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_t2).s == 1 );
                REQUIRE( d.iReg->read(Arch::Reg_t5).s == 2 );
                REQUIRE( d.iReg->read(Arch::Reg_s3).s == -14 );
                REQUIRE( d.iReg->read(Arch::Reg_s4).s == 65 );
                REQUIRE( d.fReg->read(Arch::Reg_fs2).f == Approx(1.64872).epsilon(0.00001));
                REQUIRE( d.fReg->read(Arch::Reg_fs3).f == Approx(1.68000).epsilon(0.001));
            }
        }
        WHEN( "run multiply accumulate program" ) {
            inputFile = fopen("samples/mult_acc.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_v0).s == 14 );
            }
        }
        WHEN( "run line intercept program" ) {
            inputFile = fopen("samples/intercept.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.iReg->read(Arch::Reg_a0).s == 20);
                REQUIRE( d.iReg->read(Arch::Reg_t0).s == 120);
                REQUIRE( d.iReg->read(Arch::Reg_t3).s == 120);
            }
        }
        WHEN( "run eulers method program" ) {
            inputFile = fopen("samples/eulers.bin", "rb");
            d.loadProgram(inputFile);
            d.go();
            THEN( "check register file" ) {
                REQUIRE( d.fReg->read(Arch::Reg_fv0).f == Approx(0.89).epsilon(0.0001) );
                REQUIRE( d.fReg->read(Arch::Reg_fv1).f == Approx(5.995187).epsilon(0.0001) );
                // REQUIRE( d.iReg->read(Arch::Reg_a0).s == 5.996983);
            }
        }
    }
}
