/***************************************************************************//**
    \file Instructions.cpp
    \brief Interface to the instructions that comprise the core ISA.
*//****************************************************************************/
#include "Instructions.hpp"
#include "Datapath.hpp"

namespace Insts {

/***************************************************************************//**
	This method is called by Instruction Fetch. However, it is placed here to
	provide access to all of the instruction sub classes.

	@param datapath - The datapath in which the instruction is executing.
	@return A pointer to the instruction.
*//****************************************************************************/
Inst* InstFactoryMethod(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction) {
	Arch::Op_t opcode = Util::getOp(rawInstruction);
	switch (opcode) {
		case Arch::Op_nop: return new Nop(datapath, pc, rawInstruction);
		case Arch::Op_brk: return new Brk(datapath, pc, rawInstruction);
		case Arch::Op_add: return new Add(datapath, pc, rawInstruction);
		case Arch::Op_sub: return new Sub(datapath, pc, rawInstruction);
		case Arch::Op_mul: return new Mul(datapath, pc, rawInstruction);
		case Arch::Op_div: return new Div(datapath, pc, rawInstruction);
		case Arch::Op_and: return new And(datapath, pc, rawInstruction);
		case Arch::Op_or:  return new Or(datapath, pc, rawInstruction);
		case Arch::Op_xor: return new Xor(datapath, pc, rawInstruction);
		case Arch::Op_shl: return new Shl(datapath, pc, rawInstruction);
		case Arch::Op_shr: return new Shr(datapath, pc, rawInstruction);
		case Arch::Op_slt: return new Slt(datapath, pc, rawInstruction);
		case Arch::Op_addi:return new Addi(datapath, pc, rawInstruction);
		case Arch::Op_andi:return new Andi(datapath, pc, rawInstruction);
		case Arch::Op_ori: return new Ori(datapath, pc, rawInstruction);
		case Arch::Op_slti:return new Slti(datapath, pc, rawInstruction);
		case Arch::Op_loi: return new Loi(datapath, pc, rawInstruction);
		case Arch::Op_hii: return new Hii(datapath, pc, rawInstruction);
		case Arch::Op_lw:  return new Lw(datapath, pc, rawInstruction);
		case Arch::Op_sw:  return new Sw(datapath, pc, rawInstruction);
		case Arch::Op_beq: return new Beq(datapath, pc, rawInstruction);
		case Arch::Op_bne: return new Bne(datapath, pc, rawInstruction);
		case Arch::Op_j:   return new J(datapath, pc, rawInstruction);
		case Arch::Op_jal: return new Jal(datapath, pc, rawInstruction);
		case Arch::Op_jr:  return new Jr(datapath, pc, rawInstruction);
		case Arch::Op_jrl: return new Jrl(datapath, pc, rawInstruction);
		case Arch::Op_addf:return new Addf(datapath, pc, rawInstruction);
		case Arch::Op_subf:return new Subf(datapath, pc, rawInstruction);
		case Arch::Op_mulf:return new Mulf(datapath, pc, rawInstruction);
		case Arch::Op_divf:return new Divf(datapath, pc, rawInstruction);
		case Arch::Op_sltf:return new Sltf(datapath, pc, rawInstruction);
		case Arch::Op_lof: return new Lof(datapath, pc, rawInstruction);
		case Arch::Op_hif: return new Hif(datapath, pc, rawInstruction);
		case Arch::Op_lwf: return new Lwf(datapath, pc, rawInstruction);
		case Arch::Op_swf: return new Swf(datapath, pc, rawInstruction);
		case Arch::Op_beqf:return new Beqf(datapath, pc, rawInstruction);
		case Arch::Op_bnef:return new Bnef(datapath, pc, rawInstruction);
		case Arch::Op_mod: return new Mod(datapath, pc, rawInstruction);
		case Arch::Op_mac: return new Mac(datapath, pc, rawInstruction);
		case Arch::Op_crv: return new Crv(datapath, pc, rawInstruction);
		case Arch::Op_exp: return new Exp(datapath, pc, rawInstruction);
		case Arch::Op_cas: return new Cas(datapath, pc, rawInstruction);
		case Arch::Op_halt:return new Halt(datapath, pc, rawInstruction);
		default: assert(false && "Missing instruction opcode.");
	}
}

/***************************************************************************//**
	R type instruction constructor.
*//****************************************************************************/
RInst::RInst(Datapath::Datapath* datapath,
			 Arch::Address_t pc,
			 Arch::UnsignedWord_t rawInstruction,
	         Arch::Op_t opcode,
	         Eu::Op_t euOp)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Alu,
		   euOp,
		   Mem::Mode_Pass,
		   Util::getReg2(rawInstruction)),
	rd(wbReg),
	rs(Util::getReg0(rawInstruction)),
	rt(Util::getReg1(rawInstruction)){
}

/***************************************************************************//**
	R type instruction execution unit operand decoder.
*//****************************************************************************/
void RInst::setEuOperands(void) {
	euOperandA = datapath->iReg->read(rs);
	euOperandB = datapath->iReg->read(rt);
}

/***************************************************************************//**
	Get the instruction string for a R type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string RInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rd);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rt);
	return stream.str();
}

Arch::Address_t Jr::getNextPcId(void) {
	if (datapath->config.controlChangeDetectionInDecode) {
		return euOperandA.u;
	} else {
		return Inst::getNextPcId();
	}
};

Arch::Address_t Jrl::getNextPcId(void) {
	if (datapath->config.controlChangeDetectionInDecode) {
		return euOperandA.u;
	} else {
		return Inst::getNextPcId();
	}
};

/***************************************************************************//**
	I type instruction constructor.
*//****************************************************************************/
IInst::IInst(Datapath::Datapath* datapath,
			 Arch::Address_t pc,
			 Arch::UnsignedWord_t rawInstruction,
	         Arch::Op_t opcode,
	         Eu::Op_t euOp)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Alu,
		   euOp,
		   Mem::Mode_Pass,
		   Util::getReg1(rawInstruction)),
	rd(wbReg),
	rs(Util::getReg0(rawInstruction)),
	im(Util::getIm(rawInstruction)){
}

/***************************************************************************//**
	I type instruction execution unit operand decoder.
*//****************************************************************************/
void IInst::setEuOperands(void) {
	euOperandA = datapath->iReg->read(rs);
	euOperandB = Util::unsignedWord(Util::signExtend(im));
}

/***************************************************************************//**
	Get the instruction string for a I type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string IInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rd);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
	stream << "0x" << std::right << std::setfill('0') << std::setw(4) << std::hex << im;
	return stream.str();
}

void Hii::setEuOperands(void) {
	euOperandA = Util::unsignedWord(im << 16);
	euOperandB = datapath->iReg->read(rd);
}

/***************************************************************************//**
	M type instruction constructor.
*//****************************************************************************/
MInst::MInst(Datapath::Datapath* datapath,
			 Arch::Address_t pc,
			 Arch::UnsignedWord_t rawInstruction,
	         Arch::Op_t opcode,
	         Mem::Mode_t memMode,
		     Arch::Reg_t wbReg)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Alu,
		   Eu::Op_Add,
		   memMode,
		   wbReg),
	rd(Util::getReg1(rawInstruction)),
	rs(Util::getReg0(rawInstruction)),
	im(Util::getIm(rawInstruction)){
}

/***************************************************************************//**
	M type instruction execution unit operand decoder.
*//****************************************************************************/
void MInst::setEuOperands(void) {
	euOperandA = datapath->iReg->read(rs);
	euOperandB = Util::unsignedWord((Arch::SignedWord_t) (int16_t) im);
}

/***************************************************************************//**
	Get the instruction string for a M type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string MInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rd);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
	stream << "0x" << std::right << std::setfill('0') << std::setw(4) << std::hex << im;
	return stream.str();
}

void Sw::setMemWriteData(void) { memWriteData = datapath->iReg->read(rd); }

/***************************************************************************//**
	B type instruction constructor.
*//****************************************************************************/
BInst::BInst(Datapath::Datapath* datapath,
			 Arch::Address_t pc,
			 Arch::UnsignedWord_t rawInstruction,
	         Arch::Op_t opcode)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Alu,
		   Eu::Op_Subtract,
		   Mem::Mode_Pass,
		   Arch::Reg_r0),
	rs(Util::getReg0(rawInstruction)),
	rt(Util::getReg1(rawInstruction)),
	im(Util::getIm(rawInstruction)){
	predictedBranch = datapath->bp->predict(this);
}

/***************************************************************************//**
	B type instruction execution unit operand decoder.
*//****************************************************************************/
void BInst::setEuOperands(void) {
	euOperandA = datapath->iReg->read(rs);
	euOperandB = datapath->iReg->read(rt);
}

Arch::Address_t BInst::getNextPcIf(void) {
	if (datapath->config.controlChangeDetectionInFetch) {
		return (predictedBranch == Bp::Prediction_Taken) ? (pc + Util::signExtend(im)) : getNextPcDefault();
	} else {
		return Inst::getNextPcIf();
	}
};

Arch::Address_t BInst::getNextPcId(void) {
	if (datapath->config.controlChangeDetectionInDecode) {
		return (predictedBranch == Bp::Prediction_Taken) ? (pc + Util::signExtend(im)) : getNextPcDefault();
	} else {
		return Inst::getNextPcId();
	}
};

/***************************************************************************//**
	Get the instruction string for a B type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string BInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rt);
	stream << "0x" << std::right << std::setfill('0') << std::setw(4) << std::hex << im;
	return stream.str();
}

/***************************************************************************//**
	J type instruction constructor.
*//****************************************************************************/
JInst::JInst(Datapath::Datapath* datapath,
			 Arch::Address_t pc,
			 Arch::UnsignedWord_t rawInstruction,
	         Arch::Op_t opcode,
	         Arch::Reg_t wbReg)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Alu,
		   Eu::Op_Add,
		   Mem::Mode_Pass,
		   wbReg),
	addr(Util::getAddr(rawInstruction)) {
}

Arch::Address_t JInst::getNextPcIf(void) {
	if (datapath->config.controlChangeDetectionInFetch) {
		return addr;
	} else {
		return Inst::getNextPcIf();
	}
};

Arch::Address_t JInst::getNextPcId(void) {
	if (datapath->config.controlChangeDetectionInDecode) {
		return addr;
	} else {
		return Inst::getNextPcId();
	}
};

/***************************************************************************//**
	Get the instruction string for a J type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string JInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << "0x" << std::right << std::setfill('0') << std::setw(4) << std::hex << addr;
	return stream.str();
}

/***************************************************************************//**
	FR type instruction constructor.
*//****************************************************************************/
FRInst::FRInst(Datapath::Datapath* datapath,
			   Arch::Address_t pc,
			   Arch::UnsignedWord_t rawInstruction,
	           Arch::Op_t opcode,
	           Eu::Op_t euOp)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Fpu,
		   euOp,
		   Mem::Mode_Pass,
		   Util::getReg2(rawInstruction)),
	rd(wbReg),
	rs(Util::getReg0(rawInstruction)),
	rt(Util::getReg1(rawInstruction)){
	exCycles = datapath->config.fpuNumCycles;
}

/***************************************************************************//**
	FR type instruction execution unit operand decoder.
*//****************************************************************************/
void FRInst::setEuOperands(void) {
	euOperandA = datapath->fReg->read(rs);
	euOperandB = datapath->fReg->read(rt);
}

/***************************************************************************//**
	Get the instruction string for a FR type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string FRInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rd);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rs);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rt);
	return stream.str();
}

/***************************************************************************//**
	FI type instruction constructor.
*//****************************************************************************/
FIInst::FIInst(Datapath::Datapath* datapath,
			   Arch::Address_t pc,
			   Arch::UnsignedWord_t rawInstruction,
	           Arch::Op_t opcode,
	           Eu::Op_t euOp)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Alu,
		   euOp,
		   Mem::Mode_Pass,
		   Util::getReg1(rawInstruction)),
	rd(wbReg),
	rs(Util::getReg0(rawInstruction)),
	im(Util::getIm(rawInstruction)){
}

/***************************************************************************//**
	Get the instruction string for a FI type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string FIInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rd);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rs);
	stream << "0x" << std::right << std::setfill('0') << std::setw(4) << std::hex << im;
	return stream.str();
}

void Hif::setEuOperands(void) {
	euOperandA = Util::unsignedWord(im << 16);
	euOperandB = datapath->fReg->read(rd);
}

/***************************************************************************//**
	FM type instruction constructor.
*//****************************************************************************/
FMInst::FMInst(Datapath::Datapath* datapath,
			   Arch::Address_t pc,
			   Arch::UnsignedWord_t rawInstruction,
	           Arch::Op_t opcode,
	           Mem::Mode_t memMode,
		       Arch::Reg_t wbReg)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Alu,
		   Eu::Op_Add,
		   memMode,
		   wbReg),
	rd(Util::getReg1(rawInstruction)),
	rs(Util::getReg0(rawInstruction)),
	im(Util::getIm(rawInstruction)){
}

/***************************************************************************//**
	FM type instruction execution unit operand decoder.
*//****************************************************************************/
void FMInst::setEuOperands(void) {
	euOperandA = datapath->iReg->read(rs);
	euOperandB = Util::unsignedWord((Arch::SignedWord_t) (int16_t) im);
}

/***************************************************************************//**
	Get the instruction string for a FM type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string FMInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rd);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
	stream << "0x" << std::right << std::setfill('0') << std::setw(4) << std::hex << im;
	return stream.str();
}

void Swf::setMemWriteData(void) {
	memWriteData = datapath->fReg->read(rd);
}

/***************************************************************************//**
	FB type instruction constructor.
*//****************************************************************************/
FBInst::FBInst(Datapath::Datapath* datapath,
			   Arch::Address_t pc,
			   Arch::UnsignedWord_t rawInstruction,
	           Arch::Op_t opcode)
	: Inst(datapath,
		   pc,
		   rawInstruction,
		   opcode,
		   Eu::Type_Fpu,
		   Eu::Op_Subtract,
		   Mem::Mode_Pass,
		   Arch::Reg_r0),
	rs(Util::getReg0(rawInstruction)),
	rt(Util::getReg1(rawInstruction)),
	im(Util::getIm(rawInstruction)){
	exCycles = datapath->config.fpuNumCycles;
	predictedBranch = datapath->bp->predict(this);
}

/***************************************************************************//**
	FB type instruction execution unit operand decoder.
*//****************************************************************************/
void FBInst::setEuOperands(void) {
	euOperandA = datapath->fReg->read(rs);
	euOperandB = datapath->fReg->read(rt);
}

Arch::Address_t FBInst::getNextPcIf(void) {
	if (datapath->config.controlChangeDetectionInFetch) {
		return (predictedBranch == Bp::Prediction_Taken) ? (pc + Util::signExtend(im)) : getNextPcDefault();
	} else {
		return Inst::getNextPcIf();
	}
};

Arch::Address_t FBInst::getNextPcId(void) {
	if (datapath->config.controlChangeDetectionInDecode) {
		return (predictedBranch == Bp::Prediction_Taken) ? (pc + Util::signExtend(im)) : getNextPcDefault();
	} else {
		return Inst::getNextPcId();
	}
};

/***************************************************************************//**
	Get the instruction string for a FB type instruction.

	@return The instruction string.
*//****************************************************************************/
std::string FBInst::getInstructionStr(void) {
	std::stringstream stream;
	stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rs);
	stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rt);
	stream << "0x" << std::right << std::setfill('0') << std::setw(4) << std::hex << im;
	return stream.str();
}

}
