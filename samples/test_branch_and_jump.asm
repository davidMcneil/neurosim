	.text
addi t0 r0 11
addi t1 r0 11
addi t2 r0 0
lof ft0 0x3333
hif ft0 0x3f33
addf ft1 ft0 fr0
addf ft2 ft0 fr0

j b1:

tmp1:
addi t1 t1 1
addi t2 t2 1
j b1:

tmp2:
addi t1 r0 11
addi t2 t2 1
j b2:

tmp3:
addf ft1 ft1 ft0
addi t2 t2 1
j b3:

tmp4:
addf ft1 ft0 fr0
addi t2 t2 1
j b4:

b1:
beq t0 t1 tmp1:

b2:
bne t0 t1 tmp2:

b3:
beqf ft1 ft2 tmp3:

b4:
bnef ft1 ft2 tmp4:

jal funct:
addi t3 r0 15
loi t5 funct:
hii t5 funct:
jrl t5
addi t3 t3 -11
j end:

funct:
	addi t4 t4 21
	jr ra

# t0 = 11
# t1 = 11
# ft1 = 0.7
# ft2 = 0.7
# t2 = 4
# ra = 116
# t3 = 4
# t4 = 42

end:
halt

	.data
