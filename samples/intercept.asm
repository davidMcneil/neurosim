	.text
addi a0 r0 0  # X value

addi t1 r0 6  # slope 1
addi s1 r0 0  # Y intercept 1
add  t0 r0 s1 # Y value 1

addi t4 r0 5  # slope 2
addi s4 r0 20 # Y intercept 2
add  t3 r0 s4 # Y value 2

main:
	# Check if the lines have intercepted
	beq t0 t3 end:
	jal inc:
	j   main:

inc:
	addi a0 a0 1
	mul  t7 a0 t1
	mul  t8 a0 t4
	add  t0 s1 t7
	add  t3 s4 t8
	jr   ra

end:
halt

	.data
