	.text
addi t0 r0 0
addi t1 r0 0
addi t2 r0 128

save:
	beq  t0 t2 read:
	sw   t0 t0 0
	addi t0 t0 1
	j    save:

read:
	beq  t1 t2 end:
	lw   t3 t1 0
	addi t1 t1 1
	j    read:

end:
halt

	.data
