	.text
main:
	addi a0 r0 1
	addi a1 r0 2
	addi a2 r0 3
	addi a3 r0 4
	jal helper:
	j end:

helper:
	mul t0 a0 a1
	mul t1 a2 a3
	add v0 t0 t1
	jr ra

end:
	add v0 v0 r0
	halt

	.data
