	.text
# R Type Instruction Setup
addi t0 r0 17
addi t1 r0 31
loi  t2 0xFFFD
hii  t2 0xFFFF # -3
addi t3 r0 5
addi t4 r0 2
# R Type Instruction Execution
add  s0 t0 t2 # 14
sub  s1 t2 t1 # -34
mul  s2 t0 t1 # 527
div  s3 t1 t2 # -10
and  s4 t0 t3 # 1
or   s5 t1 t2 # -1
xor  s6 t2 t3 # -8
shl  s7 t2 t3 # -96
shr  t5 t1 t4 # 7
slt  t6 t1 t0 # 0
slt  t7 t2 t4 # 1
halt

	.data
