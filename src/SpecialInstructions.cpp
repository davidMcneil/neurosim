/***************************************************************************//**
    \file SpcialInstructions.cpp
    \brief Interface to the special instructions in the ISA.
*//****************************************************************************/
#include "SpecialInstructions.hpp"
#include "Datapath.hpp"

namespace Insts {

/***************************************************************************//**
    S type instruction constructor.
*//****************************************************************************/
SInst::SInst(Datapath::Datapath* datapath,
             Arch::Address_t pc,
             Arch::UnsignedWord_t rawInstruction,
             Arch::Op_t opcode,
             Eu::Op_t euOp,
             Mem::Mode_t memMode,
             Arch::Reg_t wbReg)
    : Inst(datapath,
           pc,
           rawInstruction,
           opcode,
           Eu::Type_Spu,
           euOp,
           memMode,
           wbReg) {
    exCycles = datapath->config.spuNumCycles;
}

/***************************************************************************//**
    Mod special operation.
*//****************************************************************************/
Mod::Mod(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
    : SInst(datapath,
            pc,
            rawInstruction,
            Arch::Op_mod,
            Eu::Op_Mod,
            Mem::Mode_Pass,
            Util::getReg2(rawInstruction)),
    rd(wbReg),
    rs(Util::getReg0(rawInstruction)),
    rt(Util::getReg1(rawInstruction)) {

};

void Mod::setEuOperands(void) {
    euOperandA = datapath->iReg->read(rs);
    euOperandB = datapath->iReg->read(rt);
}

std::string Mod::getInstructionStr(void) {
    std::stringstream stream;
    stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rd);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rt);
    return stream.str();
}

/***************************************************************************//**
    Cas special operation.
*//****************************************************************************/
Cas::Cas(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
    : SInst(datapath,
            pc,
            rawInstruction,
            Arch::Op_cas,
            Eu::Op_Add,
            Mem::Mode_Special,
            Arch::Reg_r0),
    rs(Util::getReg0(rawInstruction)),
    rt(Util::getReg1(rawInstruction)) {

};

Mem::Transaction_t Cas::memSpecialTransaction(void) {
    Mem::Transaction_t currentTransaction;
    Arch::Address_t address1 = datapath->iReg->read(rs).u;
    Arch::Address_t address2 = datapath->iReg->read(rt).u;
    Mem::Transaction_t transaction1 = datapath->mem->readWithCache(address1);
    Mem::Transaction_t transaction2 = datapath->mem->readWithCache(address2);
    /** Select the transaction that has a miss if there is one */
    if (transaction1.status == Mem::CacheStatus_Miss) {
        currentTransaction = transaction1;
    } else {
        currentTransaction = transaction2;
    }
    /** Compare and swap if necessary */
    if (transaction1.data.s > transaction2.data.s) {
        datapath->mem->writeWithCache(address1, transaction2.data);
        datapath->mem->writeWithCache(address2, transaction1.data);
    }
    return currentTransaction;
}

std::string Cas::getInstructionStr(void) {
    std::stringstream stream;
    stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rt);
    return stream.str();
}

/***************************************************************************//**
    Mac special operation.
*//****************************************************************************/
Mac::Mac(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
    : SInst(datapath,
            pc,
            rawInstruction,
            Arch::Op_mac,
            Eu::Op_Add,
            Mem::Mode_Pass,
            Util::getReg2(rawInstruction)),
    rd(wbReg),
    rs(Util::getReg0(rawInstruction)),
    rt(Util::getReg1(rawInstruction)) {

};

void Mac::setEuOperands(void) {
    euOperandA = datapath->iReg->read(rd);
    euOperandB = Util::signedWord(datapath->iReg->read(rs).s *
                                  datapath->iReg->read(rt).s);
}

std::string Mac::getInstructionStr(void) {
    std::stringstream stream;
    stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rd);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rs);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->iReg->getStr(rt);
    return stream.str();
}

/***************************************************************************//**
    Crv special operation.
*//****************************************************************************/
Crv::Crv(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
    : SInst(datapath,
            pc,
            rawInstruction,
            Arch::Op_crv,
            Eu::Op_Crv,
            Mem::Mode_Pass,
            Util::getReg2(rawInstruction)),
    rd(wbReg),
    rs(Util::getReg0(rawInstruction)),
    rt(Util::getReg1(rawInstruction)) {

};

void Crv::setEuOperands(void) {
    euOperandA = datapath->fReg->read(rs);
    euOperandB = datapath->fReg->read(rt);
}

std::string Crv::getInstructionStr(void) {
    std::stringstream stream;
    stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rd);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rs);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rt);
    return stream.str();
}

/***************************************************************************//**
    Exp special operation.
*//****************************************************************************/
Exp::Exp(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
    : SInst(datapath,
            pc,
            rawInstruction,
            Arch::Op_exp,
            Eu::Op_Exp,
            Mem::Mode_Pass,
            Util::getReg2(rawInstruction)),
    rd(wbReg),
    rs(Util::getReg0(rawInstruction)),
    rt(Util::getReg1(rawInstruction)) {

};

void Exp::setEuOperands(void) {
    euOperandA = datapath->fReg->read(rs);
    euOperandB = datapath->fReg->read(rt);
}

std::string Exp::getInstructionStr(void) {
    std::stringstream stream;
    stream << std::left << std::setfill(' ') << std::setw(5) << getOpcodeStr();
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rd);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rs);
    stream << std::left << std::setfill(' ') << std::setw(4) << datapath->fReg->getStr(rt);
    return stream.str();
}

}
