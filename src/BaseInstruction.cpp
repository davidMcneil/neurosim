/***************************************************************************//**
    \file BassInstruction.cpp
    \brief The base insturction class.
*//****************************************************************************/
#include "BaseInstruction.hpp"
#include "Datapath.hpp"

namespace Insts {

/***************************************************************************//**
    Instruction base class constructor. Tasks to be done during instruction fetch
    should be done here as this constructor is called in the factory method
    constructor IF.
*//****************************************************************************/
Inst::Inst(Datapath::Datapath* datapath,
           Arch::Address_t pc,
           Arch::UnsignedWord_t rawInstruction,
           Arch::Op_t opcode,
           Eu::Type_t euType,
           Eu::Op_t euOp,
           Mem::Mode_t memMode,
           Arch::Reg_t wbReg)
    : datapath(datapath),
      pc(pc),
      nextPc(getNextPcIf()),
      predictedBranch(Bp::Prediction_None),
      actualBranch(Bp::Prediction_None),
      rawInstruction(rawInstruction),
      opcode(opcode),
      dependentRegs(),
      exCycles(1),
      euType(euType),
      euOp(euOp),
      euOperandA(Util::zeroWord),
      euOperandB(Util::zeroWord),
      euResult(Eu::defaultResult),
      memCycles(1),
      memMode(memMode),
      memAddress(0),
      memWriteData(Util::zeroWord),
      memTransaction(Mem::defaultTransaction),
      wbReg(wbReg),
      wbData(Util::zeroWord) {
    predictedBranch = datapath->bp->predict(this);
}

/***************************************************************************//**
    Instruction Fetch.
    -Fetch the next instruction from memory
    -Generate the unique instruction given the opcode, acts as factory method
    -Update next PC based on IF update PC logic (done in Inst constructor)
    -Perform branch prediction (done in Inst constructor)

    @param datapath - The datapath in which the instruction is executing.
    @return A pointer to the instruction.
*//****************************************************************************/
Inst* Inst::IF(Datapath::Datapath* datapath){
    return InstFactoryMethod(datapath, datapath->pc, datapath->mem->read(datapath->pc).data.u);
}

/***************************************************************************//**
    Default execution operand decoder. Simply sets both operands to 0.
*//****************************************************************************/
void Inst::setEuOperands(void) {
    euOperandA = Util::unsignedWord(0);
    euOperandB = Util::unsignedWord(0);
}

/***************************************************************************//**
    Default memory write data setter. Simply data to 0.
*//****************************************************************************/
void Inst::setMemWriteData(void) {
    memWriteData = Util::unsignedWord(0);
}

/***************************************************************************//**
    Instruction decode.
    -Set the registers the current instruction depends on
    -Set execution unit operands
    -Set the memory write data
    -Update next PC based on ID update PC logic

    @return A pointer to the instruction.
*//****************************************************************************/
Inst* Inst::ID(void) {
    setDependentRegs();
    setEuOperands();
    setMemWriteData();
    nextPc = getNextPcId();
    return this;
}

/***************************************************************************//**
    Instruction execute.
    -Execute the instructions exectuion unit
    -Set the memory address to the output of the execution unit
    -Set the actual branch
    -Updates the next PC based on EX update PC logic
    -Decrement the number of exectuion cycles
    -Update the branch predictor based on the actual branch

    @return A pointer to the instruction.
*//****************************************************************************/
Inst* Inst::EX(void) {
    euResult = Eu::execute(euType, euOp, euOperandA, euOperandB);
    memAddress = ((Arch::Address_t) euResult.result.u);
    setActualBranch();
    nextPc = getNextPcEx();
    if (exCycles > 0) {
        exCycles--;
    }
    if (exCycles == 0) {
        /** Only update the branch predictions the last time through execute. */
        datapath->bp->update(this);
    }
    assert(exCycles >= 0 && "Negative number of execution cycles remaining.");
    return this;
}

/***************************************************************************//**
    Instruction memory.
    -Perform the memory transaction corresponding to the instructions memory mode.
    -Check for cache miss penalty
    -Decrement the number of memory cycles

    @return A pointer to the instruction.
*//****************************************************************************/
Inst* Inst::MEM(void){
    Mem::Transaction_t currentTransaction;
    switch(memMode) {
        case Mem::Mode_Read:
            currentTransaction = datapath->mem->readWithCache(memAddress);
            break;
        case Mem::Mode_Write:
            currentTransaction = datapath->mem->writeWithCache(memAddress, memWriteData);
            break;
        case Mem::Mode_Pass:
            currentTransaction = Mem::defaultTransaction;
            break;
        case Mem::Mode_Special:
            currentTransaction = memSpecialTransaction();
            break;
        default:
            assert(false && "Unsuporrted memory mode.");
    }
    switch(currentTransaction.status) {
        case Mem::CacheStatus_Hit:
            /** Update transaction if this is the first time in the mem stage. */
            if (memTransaction.status == Mem::CacheStatus_None) {
                memTransaction = currentTransaction;
            }
            break;
        case Mem::CacheStatus_Miss:
            /** Update transaction if this is the first time in the mem stage. */
            if (memTransaction.status == Mem::CacheStatus_None) {
                /** Apply the cache miss penalty */
                memCycles = memCycles + datapath->config.cacheMissCyclePenalty;
                memTransaction = currentTransaction;
            }
            break;
        case Mem::CacheStatus_None:
            memTransaction = currentTransaction;
            break;
        default:
            assert(false && "Unsuporrted cache status.");
    }
    if (memCycles > 0) {
        memCycles--;
    }
    assert(memCycles >= 0 && "Negative number of memory cycles remaining.");
    return this;
}

/***************************************************************************//**
    Default write back data setter. Write back data equals the result of the
    execution unit.
*//****************************************************************************/
void Inst::setWbData(void) {
    wbData = euResult.result;
}

/***************************************************************************//**
    Instruction write back.
    -Set the writeback data
    -Writeback the data to the write back register

    @return A pointer to the instruction.
*//****************************************************************************/
Inst* Inst::WB(void) {
    setWbData();
    if (isFpInst()) {
        datapath->fReg->write(wbReg, wbData);
    } else {
        datapath->iReg->write(wbReg, wbData);
    }
    return this;
}

/***************************************************************************//**
    Helper used to get instruction string.
*//****************************************************************************/
void writeLabeledHexToStream(std::stringstream* stream, std::string label, Arch::UnsignedWord_t value) {
    *stream << "| " << std::left << std::setfill(' ') << std::setw(12) << label;
    *stream << "0x" << std::hex << std::right << std::setfill('0') << std::setw(8) << value;
    *stream << std::setfill(' ') << std::setw(1) << "" << " |\n";
}

/***************************************************************************//**
    Helper used to get instruction string.
*//****************************************************************************/
void writeLabeledIntToStream(std::stringstream* stream, std::string label, Arch::SignedWord_t value) {
    *stream << "| " << std::left << std::setfill(' ') << std::setw(12) << label;
    *stream << std::dec << std::left << std::setfill(' ') << std::setw(11) << value;
    *stream << std::setfill(' ') << std::setw(0) << "" << " |\n";
}

/***************************************************************************//**
    Helper used to get instruction string.
*//****************************************************************************/
void writeLabeledFloatToStream(std::stringstream* stream, std::string label, Arch::FloatWord_t value) {
    *stream << "| " << std::left << std::setfill(' ') << std::setw(12) << label;
    *stream << std::dec << std::left << std::setfill(' ') << std::setw(11) << value;
    *stream << std::setfill(' ') << std::setw(0) << "" << " |\n";
}

/***************************************************************************//**
    Helper used to get instruction string.
*//****************************************************************************/
void writeLabeledStrToStream(std::stringstream* stream, std::string label, std::string value) {
    *stream << "| " << std::left << std::setfill(' ') << std::setw(12) << label;
    *stream << std::dec << std::left << std::setfill(' ') << std::setw(11) << value;
    *stream << std::setfill(' ') << std::setw(0) << "" << " |\n";
}

/***************************************************************************//**
    Get a string representing the instruction.

    @return The instruction's string representation.
*//****************************************************************************/
std::string Inst::getStr(void) {
    std::stringstream stream;
    stream << "| " << "" << std::left << std::setfill(' ') << std::setw(23) << getInstructionStr() << " |\n";
    // writeLabeledHexToStream(&stream, "Addr:", reinterpret_cast<std::uintptr_t>(this));
    writeLabeledHexToStream(&stream, "PC:", pc);
    writeLabeledHexToStream(&stream, "NextPC:", nextPc);
    writeLabeledStrToStream(&stream, "Predicted:", Bp::BranchPredictor::getPredictionStr(predictedBranch));
    writeLabeledStrToStream(&stream, "Actual:", Bp::BranchPredictor::getPredictionStr(actualBranch));
    writeLabeledHexToStream(&stream, "Raw:", rawInstruction);
    writeLabeledHexToStream(&stream, "ExCycles:", exCycles);
    writeLabeledStrToStream(&stream, "EuType:", Eu::getTypeStr(euType));
    writeLabeledStrToStream(&stream, "EuOp:", Eu::getOpStr(euOp));
    if (getType() == Arch::InstructionType_FR || getType() == Arch::InstructionType_FR) {
        writeLabeledFloatToStream(&stream, "EuA:", euOperandA.f);
        writeLabeledFloatToStream(&stream, "EuB:", euOperandB.f);
        writeLabeledFloatToStream(&stream, "EuOut:", euResult.result.f);
    } else if (getType() == Arch::InstructionType_FI) {
        writeLabeledHexToStream(&stream, "EuA:", euOperandA.u);
        writeLabeledHexToStream(&stream, "EuB:", euOperandB.u);
        writeLabeledHexToStream(&stream, "EuOut:", euResult.result.u);
    } else {
        writeLabeledIntToStream(&stream, "EuA:", euOperandA.s);
        writeLabeledIntToStream(&stream, "EuB:", euOperandB.s);
        writeLabeledIntToStream(&stream, "EuOut:", euResult.result.s);
    }
    writeLabeledIntToStream(&stream, "EuZero:", euResult.zero);
    writeLabeledIntToStream(&stream, "EuNegative:", euResult.negative);
    writeLabeledStrToStream(&stream, "MemMode:", Mem::Memory::getModeStr(memMode));
    writeLabeledHexToStream(&stream, "MemAddr:", memAddress);
    if (getType() == Arch::InstructionType_FM) {
        writeLabeledFloatToStream(&stream, "MemWrtData:", memWriteData.f);
        writeLabeledFloatToStream(&stream, "MemOut:", memTransaction.data.f);
    } else {
        writeLabeledIntToStream(&stream, "MemWrtData:", memWriteData.s);
        writeLabeledIntToStream(&stream, "MemOut:", memTransaction.data.s);
    }
    writeLabeledStrToStream(&stream, "WbReg:", datapath->iReg->getStr(wbReg));
    if (isFpInst()) {
        writeLabeledFloatToStream(&stream, "WbData:", wbData.f);
    } else {
        writeLabeledIntToStream(&stream, "WbData:", wbData.s);
    }
    return stream.str();
}

/***************************************************************************//**
    Get the string associated with the opcode.

    @return The string.
*//****************************************************************************/
std::string Inst::lookupOpcodeStr(Arch::Op_t opcode) {
    switch (opcode) {
        case Arch::Op_nop: return "nop";
        case Arch::Op_brk: return "brk";
        case Arch::Op_add: return "add";
        case Arch::Op_sub: return "sub";
        case Arch::Op_mul: return "mul";
        case Arch::Op_div: return "div";
        case Arch::Op_and: return "and";
        case Arch::Op_or:  return "or";
        case Arch::Op_xor: return "xor";
        case Arch::Op_shl: return "shl";
        case Arch::Op_shr: return "shr";
        case Arch::Op_slt: return "slt";
        case Arch::Op_addi:return "addi";
        case Arch::Op_andi:return "andi";
        case Arch::Op_ori: return "ori";
        case Arch::Op_slti:return "slti";
        case Arch::Op_loi: return "loi";
        case Arch::Op_hii: return "hii";
        case Arch::Op_lw:  return "lw";
        case Arch::Op_sw:  return "sw";
        case Arch::Op_beq: return "beq";
        case Arch::Op_bne: return "bne";
        case Arch::Op_j:   return "j";
        case Arch::Op_jal: return "jal";
        case Arch::Op_jr:  return "jr";
        case Arch::Op_jrl: return "jrl";
        case Arch::Op_addf:return "addf";
        case Arch::Op_subf:return "subf";
        case Arch::Op_mulf:return "mulf";
        case Arch::Op_divf:return "divf";
        case Arch::Op_sltf:return "sltf";
        case Arch::Op_lof: return "lof";
        case Arch::Op_hif: return "hif";
        case Arch::Op_lwf: return "lwf";
        case Arch::Op_swf: return "swf";
        case Arch::Op_beqf:return "beqf";
        case Arch::Op_bnef:return "bnef";
        case Arch::Op_mod: return "mod";
        case Arch::Op_crv: return "crv";
        case Arch::Op_exp: return "exp";
        case Arch::Op_mac: return "mac";
        case Arch::Op_cas: return "cas";
        case Arch::Op_halt:return "halt";
        default: assert(false && "Missing instruction opcode.");
    }
}

/***************************************************************************//**
    Determine if an instruction is a floating point instruction based on
    instruction type.

    @param type - The instruction type.
    @return Is floating point instruction?
*//****************************************************************************/
bool Inst::isFpInst(void) {
    switch (getType()) {
        case Arch::InstructionType_R:
        case Arch::InstructionType_I:
        case Arch::InstructionType_M:
        case Arch::InstructionType_B:
        case Arch::InstructionType_J:
            return false;
        case Arch::InstructionType_FR:
        case Arch::InstructionType_FI:
        case Arch::InstructionType_FB:
        case Arch::InstructionType_FM:
            return true;
        case Arch::InstructionType_S:
            switch(getOpcode()) {
                case Arch::Op_mod:
                case Arch::Op_mac:
                case Arch::Op_cas:
                    return false;
                case Arch::Op_crv:
                case Arch::Op_exp:
                    return true;
                default: assert(false && "Unsupported special instruction.");
            }
        default: assert(false && "Unsupported instruction type.");
    }
}

/***************************************************************************//**
    Determine the type of the instruction.

    @param type - The instruction type.
    @return Instruction type.
*//****************************************************************************/
Arch::InstructionType_t Inst::getType(void) {
    switch (opcode) {
        case Arch::Op_nop:
        case Arch::Op_brk:
        case Arch::Op_halt:
        case Arch::Op_add:
        case Arch::Op_sub:
        case Arch::Op_mul:
        case Arch::Op_div:
        case Arch::Op_and:
        case Arch::Op_or:
        case Arch::Op_xor:
        case Arch::Op_shl:
        case Arch::Op_shr:
        case Arch::Op_slt:
        case Arch::Op_jr:
        case Arch::Op_jrl:
            return Arch::InstructionType_R;
        case Arch::Op_addi:
        case Arch::Op_andi:
        case Arch::Op_ori:
        case Arch::Op_slti:
        case Arch::Op_loi:
        case Arch::Op_hii:
            return Arch::InstructionType_I;
        case Arch::Op_lw:
        case Arch::Op_sw:
            return Arch::InstructionType_M;
        case Arch::Op_beq:
        case Arch::Op_bne:
            return Arch::InstructionType_B;
        case Arch::Op_j:
        case Arch::Op_jal:
            return Arch::InstructionType_J;
        case Arch::Op_addf:
        case Arch::Op_subf:
        case Arch::Op_mulf:
        case Arch::Op_divf:
        case Arch::Op_sltf:
            return Arch::InstructionType_FR;
        case Arch::Op_lof:
        case Arch::Op_hif:
            return Arch::InstructionType_FI;
        case Arch::Op_lwf:
        case Arch::Op_swf:
            return Arch::InstructionType_FM;
        case Arch::Op_beqf:
        case Arch::Op_bnef:
            return Arch::InstructionType_FB;
        case Arch::Op_mod:
        case Arch::Op_mac:
        case Arch::Op_crv:
        case Arch::Op_exp:
        case Arch::Op_cas:
            return Arch::InstructionType_S;
        default: assert(false && "Missing instruction opcode.");
    }
}

}
