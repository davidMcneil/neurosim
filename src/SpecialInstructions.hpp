/***************************************************************************//**
    \file SpecialInstructions.hpp
    \brief Header for the interface to the special instructions in the core ISA.
*//****************************************************************************/
#ifndef __SPECIALINSTRUCTIONS_HPP__
#define __SPECIALINSTRUCTIONS_HPP__

#include "Util.hpp"

#include "ExecutionUnit.hpp"
#include "RegisterFile.hpp"
#include "Memory.hpp"
#include "BaseInstruction.hpp"

namespace Datapath {
	class Datapath;
}

namespace Insts {

/***************************************************************************//**
	S type instructions.
*//****************************************************************************/
class SInst : public Inst {
public:
	SInst(Datapath::Datapath* datapath,
		  Arch::Address_t pc,
		  Arch::UnsignedWord_t rawInstruction,
		  Arch::Op_t opcode,
		  Eu::Op_t euOp,
		  Mem::Mode_t memMode,
		  Arch::Reg_t wbReg);
	virtual ~SInst(){};
};

class Mod : public SInst {
public:
	Mod(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction);
	~Mod(){};
	std::string getInstructionStr(void);
protected:
	Arch::Reg_t rd; /** rd register */
	Arch::Reg_t rs; /** rs register */
	Arch::Reg_t rt; /** rt register */
	void setEuOperands(void);
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
};

class Cas : public SInst {
public:
	Cas(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction);
	~Cas(){};
	Mem::Transaction_t memSpecialTransaction(void);
	std::string getInstructionStr(void);
protected:
	Arch::Reg_t rs; /** rs register */
	Arch::Reg_t rt; /** rt register */
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
};

class Mac : public SInst {
public:
	Mac(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction);
	~Mac(){};
	std::string getInstructionStr(void);
protected:
	Arch::Reg_t rd; /** rd register */
	Arch::Reg_t rs; /** rs register */
	Arch::Reg_t rt; /** rt register */
	void setEuOperands(void);
	void setDependentRegs(void) { dependentRegs = {rd, rs, rt}; }
};

class Crv : public SInst {
public:
	Crv(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction);
	~Crv(){};
	std::string getInstructionStr(void);
protected:
	Arch::Reg_t rd; /** rd register */
	Arch::Reg_t rs; /** rs register */
	Arch::Reg_t rt; /** rt register */
	void setEuOperands(void);
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
};

class Exp : public SInst {
public:
	Exp(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction);
	~Exp(){};
	std::string getInstructionStr(void);
protected:
	Arch::Reg_t rd; /** rd register */
	Arch::Reg_t rs; /** rs register */
	Arch::Reg_t rt; /** rt register */
	void setEuOperands(void);
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
};

}

#endif /* __SPECIALINSTRUCTIONS_HPP__ */
