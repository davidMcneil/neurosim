/***************************************************************************//**
    \file RegisterFile.cpp
    \brief Integer and floating point register file interface.
*//****************************************************************************/
#include "RegisterFile.hpp"

namespace Reg {

/***************************************************************************//**
    Base register file constructor.

    @param size - The size of the register file.
*//****************************************************************************/
RegFile::RegFile(uint64_t size)
    : size(size), registers(size) {
    /** Set the first register to zero. */
    write((Arch::Reg_t) 0, Util::unsignedWord(0));
}

/***************************************************************************//**
    Insteger register file constructor.

    @param size - The size of the register file.
    @param spRegInitialValue - The intitial value of the stack pointer.
*//****************************************************************************/
IntRegFile::IntRegFile(uint64_t size, Arch::UnsignedWord_t spRegInitialValue)
    : RegFile(size) {
    /** Set the stack pointer */
    write(Arch::Reg_sp, Util::unsignedWord(spRegInitialValue));
}

/***************************************************************************//**
    Floating point register file initializer.

    @param size - The size of the register file.
*//****************************************************************************/
FpRegFile::FpRegFile(uint64_t size)
    : RegFile(size) {
}

/***************************************************************************//**
    Read from a register.

    @param reg - The register to read from.
    @return The read data.
*//****************************************************************************/
Arch::Word_t RegFile::read(Arch::Reg_t reg) {
    assert(size > (uint8_t) reg && "Register outside bounds.");
    Arch::Word_t data = registers[reg];
    if (reg == (Arch::Reg_t) 0) {
        /** Check to be sure the first register has not changed value. This works
            for both floating point vaues and integer values because zero has the
            same binary format. */
        assert(data.u == 0 && "Zero register not zero.");
    }
    return data;
}

/***************************************************************************//**
    Write to a register.

    @param reg - The register to write to.
    @param data - The data to write.
    @return The written data.
*//****************************************************************************/
Arch::Word_t RegFile::write(Arch::Reg_t reg, Arch::Word_t data) {
    assert(size > (uint8_t) reg && "Register outside bounds.");
    if (reg == (Arch::Reg_t) 0) {
        /* The first register is hardwired to 0. */
        return read((Arch::Reg_t) 0);
    }
    else {
        return (registers[reg] = data);
    }
}

/***************************************************************************//**
    Get the string representation of an integer register.

    @param reg - The register.
    @return The string.
*//****************************************************************************/
std::string IntRegFile::getStr(Arch::Reg_t reg) {
    switch (reg) {
        case Arch::Reg_r0: return "r0";
        case Arch::Reg_at: return "at";
        case Arch::Reg_v0: return "v0";
        case Arch::Reg_v1: return "v1";
        case Arch::Reg_a0: return "a0";
        case Arch::Reg_a1: return "a1";
        case Arch::Reg_a2: return "a2";
        case Arch::Reg_a3: return "a3";
        case Arch::Reg_t0: return "t0";
        case Arch::Reg_t1: return "t1";
        case Arch::Reg_t2: return "t2";
        case Arch::Reg_t3: return "t3";
        case Arch::Reg_t4: return "t4";
        case Arch::Reg_t5: return "t5";
        case Arch::Reg_t6: return "t6";
        case Arch::Reg_t7: return "t7";
        case Arch::Reg_t8: return "t8";
        case Arch::Reg_t9: return "t9";
        case Arch::Reg_s0: return "s0";
        case Arch::Reg_s1: return "s1";
        case Arch::Reg_s2: return "s2";
        case Arch::Reg_s3: return "s3";
        case Arch::Reg_s4: return "s4";
        case Arch::Reg_s5: return "s5";
        case Arch::Reg_s6: return "s6";
        case Arch::Reg_s7: return "s7";
        case Arch::Reg_k0: return "k0";
        case Arch::Reg_k1: return "k1";
        case Arch::Reg_gp: return "gp";
        case Arch::Reg_sp: return "sp";
        case Arch::Reg_fp: return "fp";
        case Arch::Reg_ra: return "ra";
        default: assert(false && "Missing register.");
    }
}

/***************************************************************************//**
    Get the string representation of a floating point register.

    @param reg - The register.
    @return The string.
*//****************************************************************************/
std::string FpRegFile::getStr(Arch::Reg_t reg) {
    switch (reg) {
        case Arch::Reg_fr0:  return "fr0";
        case Arch::Reg_fat:  return "fat";
        case Arch::Reg_fv0:  return "fv0";
        case Arch::Reg_fv1:  return "fv1";
        case Arch::Reg_fa0:  return "fa0";
        case Arch::Reg_fa1:  return "fa1";
        case Arch::Reg_fa2:  return "fa2";
        case Arch::Reg_fa3:  return "fa3";
        case Arch::Reg_ft0:  return "ft0";
        case Arch::Reg_ft1:  return "ft1";
        case Arch::Reg_ft2:  return "ft2";
        case Arch::Reg_ft3:  return "ft3";
        case Arch::Reg_ft4:  return "ft4";
        case Arch::Reg_ft5:  return "ft5";
        case Arch::Reg_ft6:  return "ft6";
        case Arch::Reg_ft7:  return "ft7";
        case Arch::Reg_ft8:  return "ft8";
        case Arch::Reg_ft9:  return "ft9";
        case Arch::Reg_ft10: return "ft10";
        case Arch::Reg_ft11: return "ft11";
        case Arch::Reg_fs0:  return "fs0";
        case Arch::Reg_fs1:  return "fs1";
        case Arch::Reg_fs2:  return "fs2";
        case Arch::Reg_fs3:  return "fs3";
        case Arch::Reg_fs4:  return "fs4";
        case Arch::Reg_fs5:  return "fs5";
        case Arch::Reg_fs6:  return "fs6";
        case Arch::Reg_fs7:  return "fs7";
        case Arch::Reg_fs8:  return "fs8";
        case Arch::Reg_fs9:  return "fs9";
        case Arch::Reg_fs10: return "fs10";
        case Arch::Reg_fs11: return "fs11";
        default: assert(false && "Missing register.");
    }
}

}
