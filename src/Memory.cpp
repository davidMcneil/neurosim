/***************************************************************************//**
    \file Memory.cpp
    \brief Memory interface.
*//****************************************************************************/

#include "Memory.hpp"

namespace Mem {

/** The default value for a memory transaction. */
Transaction_t defaultTransaction = {.address = 0, .data = Util::zeroWord, .status = CacheStatus_None};

/***************************************************************************//**
    Memory constructor.

    @param memorySize - The size of the memory in words.
    @param textSize - The size of the text porion of memory in words.
*//****************************************************************************/
Memory::Memory(uint64_t size, Arch::Address_t textAddressStart, Arch::Address_t stackAddressStart,
              uint64_t cacheSize, uint64_t cacheAssocitivity, uint64_t cacheNumBlocks)
    : size(size),
      textAddressStart(byte2wordAligned(textAddressStart)),
      textAddressEnd(byte2wordAligned(textAddressStart)),
      dataAddressStart(byte2wordAligned(textAddressStart + 1)),
      dataAddressEnd(byte2wordAligned(textAddressStart + 1)),
      stackAddressStart(byte2wordAligned(stackAddressStart)),
      stackAddressEnd(byte2wordAligned(stackAddressStart)),
      stackAddressMin(byte2wordAligned(stackAddressStart)),
      memory(size),
      cache(new Cache(cacheSize, cacheAssocitivity, cacheNumBlocks)) {
}

/***************************************************************************//**
    Read from the memory.

    @param address - The byte aligned address to read from.
    @return The memory transaction.
*//****************************************************************************/
Transaction_t Memory::read(Arch::Address_t address) {
    return readWordAligned(byte2wordAligned(address));
}

/***************************************************************************//**
    Write to the memory with the cache.

    @param address - The byte aligned address to write to.
    @param data - The data to write.
    @return The memory transaction.
*//****************************************************************************/
Transaction_t Memory::write(Arch::Address_t address, Arch::Word_t data) {
    return writeWordAligned(byte2wordAligned(address), data);
}

/***************************************************************************//**
    Read from the memory with the cache.

    @param address - The byte aligned address to read from.
    @return The memory transaction.
*//****************************************************************************/
Transaction_t Memory::readWithCache(Arch::Address_t address) {
    Transaction_t transaction = read(address);
    if (dataAddress(address)) {
        transaction.status = cache->read(byte2wordAligned(address));
    }
    return transaction;
}

/***************************************************************************//**
    Write to the memory.

    @param address - The byte aligned address to write to.
    @param data - The data to write.
    @return The memory transaction.
*//****************************************************************************/
Transaction_t Memory::writeWithCache(Arch::Address_t address, Arch::Word_t data) {
    Transaction_t transaction = write(address, data);
    if (dataAddress(address)) {
        transaction.status = cache->write(byte2wordAligned(address));
    }
    return transaction;
}

/***************************************************************************//**
    Read from the memory.

    @param address - The word aligned address to read from.
    @return The memory transaction.
*//****************************************************************************/
Transaction_t Memory::readWordAligned(Arch::Address_t address) {
    assert(size > address && "Memory out of bounds.");
    Transaction_t transaction;
    transaction.address = address;
    transaction.data = memory[address];
    transaction.status = CacheStatus_None;
    return transaction;
}

/***************************************************************************//**
    Write to the memory.

    @param address - The word aligned address to write to.
    @param data - The data to write.
    @return The memory transaction.
*//****************************************************************************/
Transaction_t Memory::writeWordAligned(Arch::Address_t address, Arch::Word_t data) {
    assert(size > address && "Memory out of bounds.");
    Transaction_t transaction;
    transaction.address = address;
    transaction.data = (memory[address] = data);
    transaction.status = CacheStatus_None;
    if (stackAddress(word2byteAligned(address))) {
        if (address < stackAddressMin) {
            stackAddressMin = address;
        }
    }
    return transaction;
}

/***************************************************************************//**
    Load a binary file into the text and data segment of memory.

    @param inputFile - The binary input file to read from.
*//****************************************************************************/
void Memory::loadTextAndData(FILE* inputFile) {
    Arch::UnsignedWord_t instructionOrData;
    uint64_t nextAddress = textAddressStart;
    stackAddressMin = stackAddressStart;
    while(fread(&instructionOrData, sizeof(Arch::UnsignedWord_t), 1, inputFile)) {
        writeWordAligned(nextAddress++, Util::unsignedWord(instructionOrData));
        /** Use the halt instruction to signifiy the end of the text segment. */
        if (Util::getOp(instructionOrData) == Arch::Op_halt) {
            break;
        }
    }
    if (nextAddress == textAddressStart) {
        textAddressEnd = textAddressStart;
    } else {
        textAddressEnd = nextAddress - 1;
    }
    dataAddressStart = textAddressEnd + 1;
    while(fread(&instructionOrData, sizeof(Arch::UnsignedWord_t), 1, inputFile)) {
        writeWordAligned(nextAddress++, Util::unsignedWord(instructionOrData));
    }
    if (nextAddress == dataAddressStart) {
        dataAddressEnd = dataAddressStart;
    } else {
        dataAddressEnd = nextAddress - 1;
    }
    stackAddressEnd = dataAddressEnd + 1;
}

/***************************************************************************//**
    Determine if the given address is in the text portion of memory.

    @param address - The byte aligned address to check.
    @return In text memory?
*//****************************************************************************/
bool Memory::textAddress(Arch::Address_t address) {
    address = byte2wordAligned(address);
    return (textAddressStart <= address && address <= textAddressEnd);
}

/***************************************************************************//**
    Determine if the given address is in the data portion of memory.

    @param address - The byte aligned address to check.
    @return In data memory?
*//****************************************************************************/
bool Memory::dataAddress(Arch::Address_t address) {
    address = byte2wordAligned(address);
    return (dataAddressStart <= address && address <= dataAddressEnd);
}

/***************************************************************************//**
    Determine if the given address is in the stack portion of memory.

    @param address - The byte aligned address to check.
    @return In stack memory?
*//****************************************************************************/
bool Memory::stackAddress(Arch::Address_t address) {
    address = byte2wordAligned(address);
    return (stackAddressEnd <= address && address <= stackAddressStart);
}

/***************************************************************************//**
    Get the string associated with the memory mode.

    @param mode - The memory mode.
    @return The string.
*//****************************************************************************/
std::string Memory::getModeStr(Mode_t mode) {
    switch(mode) {
        case Mode_Read: return "Read";
        case Mode_Write: return "Write";
        case Mode_Pass: return "Pass";
        case Mode_Special: return "Special";
        default: assert(false && "Unsupported memory mode.");
    }
}

/***************************************************************************//**
    Convert a byte aligned address to a word aligned address.

    @param address - Byte aligned address.
    @return Word aligned address.
*//****************************************************************************/
Arch::Address_t Memory::byte2wordAligned(Arch::Address_t address) {
    return (address / 4);
}

/***************************************************************************//**
    Convert a wird aligned address to a byte aligned address.

    @param address - Word aligned address.
    @return Byte aligned address.
*//****************************************************************************/
Arch::Address_t Memory::word2byteAligned(Arch::Address_t address) {
    return (address * 4);
}

/***************************************************************************//**
    Cache constructor.

    @param size - The size of the cache in words.
    @param associtivity - The associtivity of the cache.
    @param numBlocks - The number of word blocks per cache line.
*//****************************************************************************/
Cache::Cache(uint64_t size, uint64_t associtivity, uint64_t numBlocks)
    : size(size),
      associtivity(associtivity),
      numBlocks(numBlocks),
      numSets((uint64_t) size / (associtivity * numBlocks)),
      numIndexBits((uint64_t) std::log2(numSets)),
      numOffsetBits((uint64_t) std::log2(numBlocks)),
      numTagBits(WORD_LENGTH - (numIndexBits + numOffsetBits)),
      cache(numSets, std::vector<CacheAssocBlock_t>(associtivity)) {
    uint64_t s;
    uint64_t a;
    /* Set all cache valid bits to false */
    for( s = 0; s < numSets; s++ ) {
        for( a = 0; a < associtivity; a++ ) {
            cache[s][a].valid = false;
            cache[s][a].notUsedTimes = 0;
        }
    }
}

/***************************************************************************//**
    Try and read an address from the cache.
    Note: This function also updates the cache using a replacement policy.

    @param address - The word aligned address to read.
    @return The cache status.
*//****************************************************************************/
CacheStatus_t Cache::read(Arch::Address_t address) {
    uint64_t a;
    CacheAssocBlock_t* assocBlock;
    Arch::Address_t index = getIndex(address);
    Arch::Address_t tag = getTag(address);
    bool hit = false;
    /* Check the associations at the set for a tag hit and valid bit. */
    for( a = 0; a < associtivity; a++) {
        assocBlock = &cache[index][a];
        if (assocBlock->tag == tag && assocBlock->valid == true) {
            assocBlock->notUsedTimes = 0;
            hit = true;
        } else {
            assocBlock->notUsedTimes++;
        }
    }
    if (hit) {
        return CacheStatus_Hit;
    }
    /* Update the cache using a replacement policy. */
    assocBlock = &cache[index][replacementPolicy(index)];
    assocBlock->tag = tag;
    assocBlock->valid = true;
    assocBlock->notUsedTimes = 0;
    return CacheStatus_Miss;
}

/***************************************************************************//**
    Write an address to the cache.
    Note: This function also updates the cache using a replacement policy.

    @param address - The word aligned address to write.
    @return The cache state.
*//****************************************************************************/
CacheStatus_t Cache::write(Arch::Address_t address) {
    uint64_t a;
    CacheAssocBlock_t* assocBlock;
    Arch::Address_t index = getIndex(address);
    Arch::Address_t tag = getTag(address);
    bool hit = false;
    /* Check the associations at the set for a tag hit and valid bit.
       This means we simply have to update the data in the cache.*/
    for( a = 0; a < associtivity; a++) {
        assocBlock = &cache[index][a];
        if (assocBlock->tag == tag && assocBlock->valid == true) {
            assocBlock->notUsedTimes = 0;
            hit = true;
        } else {
            assocBlock->notUsedTimes++;
        }
    }
    if (hit) {
        return CacheStatus_Hit;
    }
    /* Update the cache using a replacement policy. */
    assocBlock = &cache[index][replacementPolicy(index)];
    assocBlock->tag = tag;
    assocBlock->valid = true;
    assocBlock->notUsedTimes = 0;
    return CacheStatus_Miss;
}

/***************************************************************************//**
    A random replaement policy for choosing wich association of the cache to
    replace.
    Note: This is not truly random as the modulo operator is biased.
*//****************************************************************************/
Arch::Address_t Cache::replacementPolicy(Arch::Address_t index) {
    uint64_t a;
    CacheAssocBlock_t* assocBlock;
    uint64_t leastUsedTimes = 0;
    uint64_t leastUsedIndex = 0;
    for( a = 0; a < associtivity; a++) {
        assocBlock = &cache[index][a];
        if (assocBlock->notUsedTimes > leastUsedTimes) {
            leastUsedTimes = assocBlock->notUsedTimes;
            leastUsedIndex = a;
        }
    }
    return leastUsedIndex;
}

/***************************************************************************//**
    Get the offset value.

    @param address - The address to calculate the offset for.
    @return The offset.
*//****************************************************************************/
Arch::Address_t Cache::getOffset(Arch::Address_t address) {
    Arch::Address_t mask = (0x1u << numOffsetBits) - 1;
    return (address & mask);
}

/***************************************************************************//**
    Get the index value.

    @param address - The address to calculate the index for.
    @return The index.
*//****************************************************************************/
Arch::Address_t Cache::getIndex(Arch::Address_t address) {
    Arch::Address_t offsetMask = (0x1u << (numOffsetBits)) - 1;
    Arch::Address_t mask = (((0x1u << (numIndexBits + numOffsetBits)) - 1) & ~offsetMask);
    return ((address & mask) >> numOffsetBits);
}

/***************************************************************************//**
    Get the tag value.

    @param address - The address to calculate the tag for.
    @return The index.
*//****************************************************************************/
Arch::Address_t Cache::getTag(Arch::Address_t address) {
    Arch::Address_t offsetIndexmask = ((0x1u << (numIndexBits + numOffsetBits)) - 1);
    Arch::Address_t mask = ((-1) & ~offsetIndexmask);
    return ((address & mask) >> (numIndexBits + numOffsetBits));
}

/***************************************************************************//**
    Get the string associated with the cache status.

    @param mode - The cache status.
    @return The string.
*//****************************************************************************/
std::string Cache::getStatusStr(CacheStatus_t status) {
    switch(status) {
        case CacheStatus_Hit: return "Hit";
        case CacheStatus_Miss: return "Miss";
        case CacheStatus_None: return "None";
        default: assert(false && "Unsupported cache status mode.");
    }
}

}
