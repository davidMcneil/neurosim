/***************************************************************************//**
    \file RegisterFile.hpp
    \brief Header for the integer and floating point register file interface.
*//****************************************************************************/
#ifndef __REGISTERFILE_HPP__
#define __REGISTERFILE_HPP__

#include "Util.hpp"

namespace Reg {

/***************************************************************************//**
	The base register file class.
*//****************************************************************************/
class RegFile {
public:
	RegFile(uint64_t size);
	Arch::Word_t read(Arch::Reg_t reg);
	Arch::Word_t write(Arch::Reg_t reg, Arch::Word_t data);
	virtual std::string getStr(Arch::Reg_t reg){return 0;};
private:
	uint64_t size; /** The size of the register file. */
	/** The stored register values. */
	std::vector<Arch::Word_t> registers;
};

/***************************************************************************//**
    Integer register class.
*//****************************************************************************/
class IntRegFile : public RegFile {
public:
	IntRegFile(uint64_t size, Arch::UnsignedWord_t spRegInitialValue);
	std::string getStr(Arch::Reg_t reg);
};

/***************************************************************************//**
    Floating point register class.
*//****************************************************************************/
class FpRegFile : public RegFile {
public:
	FpRegFile(uint64_t size);
	std::string getStr(Arch::Reg_t reg);
};

}

#endif /* __REGISTERFILE_HPP__ */
