/***************************************************************************//**
    \file ExecutionUnit.hpp
    \brief Header for execution unit interface.
*//****************************************************************************/
#ifndef __EXECUTIONUNIT_HPP__
#define __EXECUTIONUNIT_HPP__

#include "Util.hpp"

namespace Eu {

/** Execution unit types. */
typedef enum Type {
    Type_Alu, /** Arithmatic logic unit. */
    Type_Fpu, /** Floating point unit. */
    Type_Spu, /** Special unit. */
} Type_t;

/** Opecodes. */
typedef enum Op {
    Op_Add,
    Op_Subtract,
    Op_Multiply,
    Op_Divide,
    Op_And,
    Op_Or,
    Op_Xor,
    Op_ShiftLeft,
    Op_ShiftRight,
    Op_Mod,
    Op_Crv,
    Op_Exp,
} Op_t;

/** The output result of an execution unit */
typedef struct Result {
    Arch::Word_t result;
    bool zero;
    bool negative;
} Result_t;

extern Result_t execute(Type_t type, Op_t op, Arch::Word_t operandA, Arch::Word_t operandB);
extern std::string getOpStr(Op_t op);
extern std::string getTypeStr(Type_t op);
extern Result_t defaultResult;

}

#endif /* __EXECUTIONUNIT_HPP__ */
