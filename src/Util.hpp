/***************************************************************************//**
    \file Util.hpp
    \brief Utility resources.
*//****************************************************************************/
#ifndef __UTIL_HPP__
#define __UTIL_HPP__

#include <assert.h>
#include <cmath>
#include <set>
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <unistd.h>

#include "Architecture.h"

namespace Util {

extern Arch::Word_t zeroWord;

extern Arch::Op_t getOp(Arch::UnsignedWord_t rawInstruction);
extern Arch::Reg_t getReg0(Arch::UnsignedWord_t rawInstruction);
extern Arch::Reg_t getReg1(Arch::UnsignedWord_t rawInstruction);
extern Arch::Reg_t getReg2(Arch::UnsignedWord_t rawInstruction);
extern Arch::Immediate_t getIm(Arch::UnsignedWord_t rawInstruction);
extern Arch::Address_t getAddr(Arch::UnsignedWord_t rawInstruction);
extern bool getParity(uint32_t val);
extern Arch::Word_t unsignedWord(Arch::UnsignedWord_t word);
extern Arch::Word_t signedWord(Arch::SignedWord_t word);
extern Arch::Word_t floatWord(Arch::FloatWord_t word);
extern Arch::SignedWord_t signExtend(Arch::Immediate_t im);

}

#endif /* __UTIL_HPP__ */
