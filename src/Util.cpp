/***************************************************************************//**
    \file Util.cpp
    \brief Utility resources.
*//****************************************************************************/

#include "Util.hpp"

namespace Util {

/** The default zero word. */
Arch::Word_t zeroWord = unsignedWord(0);

/** A mask to get to get register arguments. */
#define REG_MASK                   (0x1Fu)

/** A mask to get immediate arguments. */
#define IMMEDIATE_MASK             (0xFFFFu)

/** A mask to get address arguments. */
#define ADDRESS_MASK               (0x3FFFFFF)

/***************************************************************************//**
    Get the opcode out of a raw instruction.

    @param rawInstruction - The unsigned instruction word to parse.
    @return The resulting opcode.
*//****************************************************************************/
Arch::Op_t getOp(Arch::UnsignedWord_t rawInstruction) {
   return ((Arch::Op_t) (rawInstruction >> (WORD_LENGTH - OP_LENGTH)));
}

/***************************************************************************//**
    Get the first reg argument out of a raw instruction.

    @param rawInstruction - The unsigned instruction word to parse.
    @return The resulting rd register.
*//****************************************************************************/
Arch::Reg_t getReg0(Arch::UnsignedWord_t rawInstruction) {
   return ((Arch::Reg_t) ((rawInstruction >> (RS_LENGTH +
                                              RT_LENGTH +
                                              R_UNUSED_LENGTH)) & REG_MASK));
}

/***************************************************************************//**
    Get the second reg argument out of a raw instruction.

    @param rawInstruction - The unsigned instruction word to parse.
    @return The resulting rs register.
*//****************************************************************************/
Arch::Reg_t getReg1(Arch::UnsignedWord_t rawInstruction) {
   return ((Arch::Reg_t) ((rawInstruction >> (RT_LENGTH +
                                              R_UNUSED_LENGTH)) & REG_MASK));
}

/***************************************************************************//**
    Get the third reg argument out of a raw instruction.

    @param rawInstruction - The unsigned instruction word to parse.
    @return The resulting rt register.
*//****************************************************************************/
Arch::Reg_t getReg2(Arch::UnsignedWord_t rawInstruction) {
   return ((Arch::Reg_t) (((rawInstruction >> R_UNUSED_LENGTH) & REG_MASK)));
}

/***************************************************************************//**
    Get the immediate argument out of a raw instruction.

    @param rawInstruction - The unsigned instruction word to parse.
    @return The resulting immediate.
*//****************************************************************************/
Arch::Immediate_t getIm(Arch::UnsignedWord_t rawInstruction) {
   return ((Arch::Immediate_t) rawInstruction & IMMEDIATE_MASK);
}

/***************************************************************************//**
    Get the address argument out of a raw instruction.

    @param rawInstruction - The unsigned instruction word to parse.
    @return The resulting address.
*//****************************************************************************/
Arch::Address_t getAddr(Arch::UnsignedWord_t rawInstruction) {
  return ((Arch::Address_t) rawInstruction & ADDRESS_MASK);
}

/***************************************************************************//**
    Package an unsigned word into a word union.

    @param word - The unsigned word to package.
    @return The resulting word.
*//****************************************************************************/
Arch::Word_t unsignedWord(Arch::UnsignedWord_t word) {
    Arch::Word_t w;
    w.u = word;
    return w;
}

/***************************************************************************//**
    Package a signed word into a word union.

    @param word - The signed word to package.
    @return The resulting word.
*//****************************************************************************/
Arch::Word_t signedWord(Arch::SignedWord_t word) {
    Arch::Word_t w;
    w.s = word;
    return w;
}

/***************************************************************************//**
    Package a float word into a word union.

    @param word - The float word to package.
    @return The resulting word.
*//****************************************************************************/
Arch::Word_t floatWord(Arch::FloatWord_t word) {
    Arch::Word_t w;
    w.f = word;
    return w;
}

/***************************************************************************//**
    Determine the parity of val.

    @param val - Value to calcualte parity of.
    @return Parity, false for even true for odd.
*//****************************************************************************/
bool getParity(uint32_t val) {
    bool parity = 0;
    while (val)
    {
        parity = !parity;
        val      = val & (val - 1);
    }
    return parity;
}

/***************************************************************************//**
    Signe extend an immediate.

    @param im - The immediate.
    @return Sign extended immediate
*//****************************************************************************/
Arch::SignedWord_t signExtend(Arch::Immediate_t im) {
    return (Arch::SignedWord_t) (int16_t) im;
}

}
