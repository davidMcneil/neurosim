/***************************************************************************//**
    \file BranchPredictor.hpp
    \brief Header for branch predictor interface.
*//****************************************************************************/
#ifndef __BRANCHPREDICTOR_HPP__
#define __BRANCHPREDICTOR_HPP__

#include "Util.hpp"

namespace Insts {
	class Inst;
}

namespace Bp {

/** Predition types. */
typedef enum Prediction {
    Prediction_None,     /** No branch prediction. */
    Prediction_Taken,    /** Branch taken. */
    Prediction_NotTaken, /** Branch not taken. */
} Prediction_t;

/** Possible branch predictors to use. */
typedef enum Predictor {
    Predictor_None,
    Predictor_AlwaysTaken,
    Predictor_AlwaysNotTaken,
    Predictor_ForwardTaken,
    Predictor_BackwardTaken,
    Predictor_Random,
    Predictor_Local,
    Predictor_Global
} Predictor_t;

/** Two bit saturating counter. */
typedef enum TwoBitCounter {
    TwoBitCounter_NotTakenStrongly,
    TwoBitCounter_NotTakenWeakly,
    TwoBitCounter_TakenWeakly,
    TwoBitCounter_TakenStrongly,
} TwoBitCounter_t;

/***************************************************************************//**
	The base branch predictor class.
*//****************************************************************************/
class BranchPredictor {
public:
	BranchPredictor(Predictor_t predictor);
	Prediction_t predict(Insts::Inst* inst);
	void update(Insts::Inst* inst);
	static std::string getPredictionStr(Prediction_t prediction);
	uint64_t getCorrectTaken(void) { return correctTaken; };
	uint64_t getPredictedTaken(void) { return predictedTaken; };
	uint64_t getActualTaken(void) { return actualTaken; };
	uint64_t getCorrectNotTaken(void) { return correctNotTaken; };
	uint64_t getPredictedNotTaken(void) { return predictedNotTaken; };
	uint64_t getActualNotTaken(void) { return actualNotTaken; };
private:
	Predictor_t predictor;
	Prediction_t localPredict(Arch::Address_t pc);
	Prediction_t globalPredict(void);
	void localUpdate(Arch::Address_t pc, Prediction_t actual);
	void globalUpdate(Prediction_t actual);
	static Prediction_t predictionFromTwoBitCounter(TwoBitCounter_t counter);
	static TwoBitCounter_t updateTwoBitCounter(TwoBitCounter_t counter, Prediction_t prediction);
	uint64_t correctTaken;      /** The number of taken predictions that were correct. */
	uint64_t predictedTaken;    /** The number of taken predictions. */
	uint64_t actualTaken;       /** The the number of actual taken branchs. */
	uint64_t correctNotTaken;   /** The number of taken predictions that were correct. */
	uint64_t predictedNotTaken; /** The number of taken predictions. */
	uint64_t actualNotTaken;    /** The the number of actual not taken branchs. */
	std::map<Arch::Address_t, TwoBitCounter_t> localHistory; /* Local history table. */
	TwoBitCounter_t globalHistory; /* Global history table. */
};

}

#endif /* __BRANCHPREDICTOR_HPP__ */
