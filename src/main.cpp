/***************************************************************************//**
  \file main.c
  \brief The main program.
*//****************************************************************************/

#include "Architecture.h"
#include "ConfigParser.hpp"
#include "Util.hpp"
#include "Datapath.hpp"

#define INPUT_STRING_LENGTH (100u)

/***************************************************************************//**
    The main program.

    \return status - status of the program.
*//****************************************************************************/
int main(int argc, char **argv) {
    char input[INPUT_STRING_LENGTH];
    char inputChar;
    bool interactiveFlag = false;
    bool statisticsFlag = false;
    bool registersFlag = false;
    bool dataMemoryFlag = false;
    bool traceFlag = false;
    bool helpFlag = false;
    bool inputFileFlag = false;
    FILE* inputFile;
    std::string configFileName = "";
    std::string outDirName = ".";
    Datapath::Config_t config;
    Datapath::Datapath datapath;
    FILE* statisticsFile;
    FILE* registersFile;
    FILE* dataMemoryFile;
    FILE* traceFile;
    bool endOfProgram = false;

    srand(time(NULL));

    /* Get input arguments. */
    while ((inputChar = getopt(argc, argv, "isrmtc:d:f:h")) != -1) {
        switch (inputChar) {
            case 'i':
                interactiveFlag = true;
                break;
            case 's':
                statisticsFlag = true;
                break;
            case 'r':
                registersFlag = true;
                break;
            case 'm':
                dataMemoryFlag = true;
                break;
            case 't':
                traceFlag = true;
                break;
            case 'f':
                if (optarg != NULL) {
                    /* Check if file exists */
                    if( access(optarg, F_OK) != -1 ) {
                        inputFile = fopen(optarg, "rb");
                        inputFileFlag = true;
                    }
                }
                break;
            case 'd':
                outDirName = optarg;
                break;
            case 'c':
                if (optarg != NULL) {
                    /* Check if file exists */
                    if( access(optarg, F_OK) != -1 ) {
                        configFileName = optarg;
                    } else {
                        printf("Unable to open configuration file. Using default configuration.\n");
                    }
                }
                break;
            case 'h':
                helpFlag = true;
                break;
            default:
                abort();
        }
    }

    /* Print the help message and return.*/
    if (helpFlag) {
        printf("Neurosim Simulator\n");
        printf("  -i Run the prgoram in interactive mode. [optional]\n");
        printf("  -s Save a statistics file at the end of execution. [optional]\n");
        printf("  -r Save a register file dump at the end of execution. [optional]\n");
        printf("  -m Save a data memory dump at the end of execution. [optional]\n");
        printf("  -t Save a trace file at the end of execution. [optional]\n");
        printf("  -d [directoryname] The directory to write output to. [optional]\n");
        printf("  -c [filename] The configuration file to use. [optional]\n");
        printf("  -f [filename] The program to load. [required]\n");
        printf("  -h Display this help message. [optional]\n");
        return 1;
    }

    /* If we unsuccessfully loaded an input file just return. */
    if (!inputFileFlag) {
        printf("Invalid input file.");
        return 1;
    }
    else { /* Initialize the datapath and load the program file. */
        Config::ConfigFile cfg(configFileName);
        config.controlChangeDetectionInFetch  = cfg.getValueOfKey<bool>("controlChangeDetectionInFetch",
                                                                       Datapath::defaultConfig.controlChangeDetectionInFetch);
        config.controlChangeDetectionInDecode = cfg.getValueOfKey<bool>("controlChangeDetectionInDecode",
                                                                       Datapath::defaultConfig.controlChangeDetectionInDecode);
        config.fpuNumCycles                   = cfg.getValueOfKey<uint64_t>("fpuNumCycles",
                                                                       Datapath::defaultConfig.fpuNumCycles);
        config.spuNumCycles                   = cfg.getValueOfKey<uint64_t>("spuNumCycles",
                                                                       Datapath::defaultConfig.spuNumCycles);
        config.branchPredictor                = (Bp::Predictor_t) cfg.getValueOfKey<uint64_t>("branchPredictor",
                                                                       Datapath::defaultConfig.branchPredictor);
        config.memorySize                     = cfg.getValueOfKey<uint64_t>("memorySize",
                                                                       Datapath::defaultConfig.memorySize);
        config.spStartingAddress              = cfg.getValueOfKey<uint64_t>("spStartingAddress",
                                                                       Datapath::defaultConfig.spStartingAddress);
        config.textStartingAddress            = cfg.getValueOfKey<uint64_t>("textStartingAddress",
                                                                       Datapath::defaultConfig.textStartingAddress);
        config.cacheSize                      = cfg.getValueOfKey<uint64_t>("cacheSize",
                                                                       Datapath::defaultConfig.cacheSize);
        config.cacheAssocitivity              = cfg.getValueOfKey<uint64_t>("cacheAssocitivity",
                                                                       Datapath::defaultConfig.cacheAssocitivity);
        config.cacheNumBlocks                 = cfg.getValueOfKey<uint64_t>("cacheNumBlocks",
                                                                       Datapath::defaultConfig.cacheNumBlocks);
        config.cacheMissCyclePenalty          = cfg.getValueOfKey<uint64_t>("cacheMissCyclePenalty",
                                                                       Datapath::defaultConfig.cacheMissCyclePenalty);
        datapath = Datapath::Datapath(config);
        datapath.loadProgram(inputFile);
        fclose(inputFile);
    }

    /* If in interactive mode enter an interactive loop. */
    if (interactiveFlag) {
        do { /** Interactive loop */
            printf("> ");
            scanf("%s", input);
            inputChar = input[0];  /* Only care about the first character */
            switch (inputChar) {
                case 'c':
                    datapath.stepCycle();
                    datapath.outputDatapath(stdout);
                    break;
                case 'g':
                    datapath.go();
                    datapath.outputDatapath(stdout);
                    break;
                case 'r':
                    datapath.outputIRegFile(stdout);
                    break;
                case 'f':
                    datapath.outputFRegFile(stdout);
                    break;
                case 'd':
                    break;
                default:
                    printf("Invalid input.");
                    break;
            }
        } while(inputChar != 'd');
    }
    else { /* If in noninteractive mode run to completion. */
        if (traceFlag) {
            traceFile = fopen("trace.txt", "w");
        }
        do {
            endOfProgram = datapath.stepCycle();
            if (traceFlag) {
                datapath.outputDatapath(traceFile);
            }
        } while( !endOfProgram );
        if (traceFlag) {
            fclose(traceFile);
        }
    }

    /* Output statistics. */
    if (statisticsFlag == true) {
        statisticsFile = fopen((outDirName + "/statistics.txt").c_str(), "w");
        datapath.outputStatistics(statisticsFile);
        fclose(statisticsFile);
    }

    /* Output register files. */
    if (registersFlag == true) {
        registersFile = fopen((outDirName + "/registers.txt").c_str(), "w");
        datapath.outputIRegFile(registersFile);
        datapath.outputFRegFile(registersFile);
        fclose(registersFile);
    }

    /* Output memory files. */
    if (dataMemoryFlag == true) {
        dataMemoryFile = fopen((outDirName + "/memory.txt").c_str(), "w");
        datapath.outputDataMemory(dataMemoryFile);
        fclose(dataMemoryFile);
    }
    return 0;
}
