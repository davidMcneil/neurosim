/***************************************************************************//**
    \file ExecutionUnit.c
    \brief Header for execution unit interface.
*//****************************************************************************/

#include "ExecutionUnit.hpp"

#define LOOKUP_LENGTH (8u)

namespace Eu {

Result_t alu(Op_t op, Arch::Word_t operandA, Arch::Word_t operandB);
Result_t fpu(Op_t op, Arch::Word_t operandA, Arch::Word_t operandB);
Result_t spu(Op_t op, Arch::Word_t operandA, Arch::Word_t operandB);
/** The default result of an execution unit. */
Result_t defaultResult = {.result = Util::zeroWord, .zero = true, .negative = false };

float X[LOOKUP_LENGTH] = { -1.5, -1.07142, -0.64285, -0.21428, 0.214285, 0.642857, 1.071428, 1.500000 };
float Y[LOOKUP_LENGTH] = { 0.223130 ,0.342518 ,0.525788 ,0.807117 ,1.238976 ,1.901907 ,2.919547 ,4.481689 };

/***************************************************************************//**
    Execute an execution unit unit.

    @param type - The type of execution unit to execute
    @param op - The operand.
    @param operandA - The A operand.
    @param operandB - The B operand.
    @return The result of the execution.
*//****************************************************************************/
Result_t execute(Type_t type, Op_t op, Arch::Word_t operandA, Arch::Word_t operandB) {
    switch (type) {
        case Type_Alu:
            return alu(op, operandA, operandB);
        case Type_Fpu:
            return fpu(op, operandA, operandB);
        case Type_Spu:
            return spu(op, operandA, operandB);
        default:
            assert(false && "Unsupported execution unit type.");
    }
}

/***************************************************************************//**
    Get the string associated with the execution unit operation.

    @param op - The execution unit operation.
    @return The execution unit operation string.
*//****************************************************************************/
std::string getOpStr(Op_t op) {
    switch(op) {
        case Op_Add: return "Add";
        case Op_Subtract: return "Subtract";
        case Op_Multiply: return "Multiply";
        case Op_Divide: return "Divide";
        case Op_And: return "And";
        case Op_Or: return "Or";
        case Op_Xor: return "Xor";
        case Op_ShiftLeft: return "ShiftLeft";
        case Op_ShiftRight: return "ShiftRight";
        case Op_Mod: return "Mod";
        case Op_Crv: return "Crv";
        case Op_Exp: return "Exp";
        default: assert(false && "Unsupported EU op code.");
    }
}

/***************************************************************************//**
    Get the string associated with the execution unit type.

    @param op - The execution unit type.
    @return The execution unit type string.
*//****************************************************************************/
std::string getTypeStr(Type_t type) {
    switch(type) {
        case Type_Alu: return "Alu";
        case Type_Fpu: return "Fpu";
        case Type_Spu: return "Spu";
        default: assert(false && "Unsupported EU type.");
    }
}

/***************************************************************************//**
    Execute the ALU unit.

    @param op - The operand.
    @param operandA - The A operand.
    @param operandB - The B operand.
    @return The result of the execution.
*//****************************************************************************/
Result_t alu(Op_t op, Arch::Word_t operandA, Arch::Word_t operandB) {
    Result_t result;
    switch(op) {
        case Op_Add:
            result.result.s = (operandA.s + operandB.s);
            break;
        case Op_Subtract:
            result.result.s = (operandA.s - operandB.s);
            break;
        case Op_Multiply:
            result.result.s = (operandA.s * operandB.s);
            break;
        case Op_Divide:
            if(operandB.s == 0) {
                result.result = Util::unsignedWord(0);
            } else {
                result.result.s = (operandA.s / operandB.s);
            }
            break;
        case Op_And:
            result.result.s = (operandA.s & operandB.s);
            break;
        case Op_Or:
            result.result.s = (operandA.s | operandB.s);
            break;
        case Op_Xor:
            result.result.s = (operandA.s ^ operandB.s);
            break;
        case Op_ShiftLeft:
            result.result.s = (operandA.s << operandB.s);
            break;
        case Op_ShiftRight:
            result.result.s = (operandA.s >> operandB.s);
            break;
        default:
            assert(false && "Unsupported alu op code.");
    }
    result.zero = (result.result.s == 0);
    result.negative = (result.result.s < 0);
    return result;
}

/***************************************************************************//**
    Execute the FPU unit.

    @param op - The operand.
    @param operandA - The A operand.
    @param operandB - The B operand.
    @return The result of the execution.
*//****************************************************************************/
Result_t fpu(Op_t op, Arch::Word_t operandA, Arch::Word_t operandB) {
    Result_t result;
    switch(op) {
        case Op_Add:
            result.result.f = (operandA.f + operandB.f);
            break;
        case Op_Subtract:
            result.result.f = (operandA.f - operandB.f);
            break;
        case Op_Multiply:
            result.result.f = (operandA.f * operandB.f);
            break;
        case Op_Divide:
            result.result.f = (operandA.f / operandB.f);
            break;
        default:
            assert(false && "Unsupported fpu op code.");
    }
    result.zero = (result.result.f == 0);
    result.negative = (result.result.f < 0);
    return result;
}

int factorial(int n) {
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

/***************************************************************************//**
    Execute the SPU unit.

    @param op - The operand.
    @param operandA - The A operand.
    @param operandB - The B operand.
    @return The result of the execution.
*//****************************************************************************/
Result_t spu(Op_t op, Arch::Word_t operandA, Arch::Word_t operandB) {
    Result_t result;
    uint8_t i;
    switch(op) {
        case Op_Mod:
            result.result.s = (operandA.s % operandB.s);
            break;
        case Op_Add:
            result.result.s = (operandA.s + operandB.s);
            break;
        case Op_Crv:
            for ( i = 0; i < LOOKUP_LENGTH; i++) {
                if (operandA.f <= X[i]) {
                    i--;
                    break;
                }
            }
            if (i == 0 || i == (LOOKUP_LENGTH - 1)) {
                result.result.f = Y[i];
            } else {
                result.result.f = Y[i] + (operandA.f - X[i]) * ((Y[i + 1] - Y[i]) / (X[i + 1] - X[i]));
            }
            break;
        case Op_Exp:
            result.result.f = 0;
            for (i = 0; i < 11; i++) {
                result.result.f = result.result.f + pow(operandA.f, i) / factorial(i);
            }
            break;
        default:
            assert(false && "Unsupported spu op code.");
    }
    result.zero = (result.result.u == 0);
    result.negative = (result.result.u < 0);
    return result;
}



}
