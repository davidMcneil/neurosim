/***************************************************************************//**
    \file Datapath.hpp
    \brief The RISC style datapath.
*//****************************************************************************/
#ifndef __DATAPATH_HPP__
#define __DATAPATH_HPP__

#include "Util.hpp"

#include "BranchPredictor.hpp"
#include "ExecutionUnit.hpp"
#include "RegisterFile.hpp"
#include "Memory.hpp"
#include "Instructions.hpp"

namespace Datapath {

/** Configure the parameters of a datapath. */
typedef struct Config {
	/************************Control Change Parameters**************************//**
	    By default control change is done in the execute stage. JR and JRL control
	    change must be done after decode as it involves reading from a register file.
	*//****************************************************************************/
	bool controlChangeDetectionInFetch;  /** Detect control changes in fetch stage of pipeline. */
	bool controlChangeDetectionInDecode; /** Detect control changes in fetch stage of pipeline. */

	/****************************Execute Parameters*****************************//**
	*//****************************************************************************/
	uint64_t fpuNumCycles; /** The number of cycles it takes the FPU unit to execute. */
	uint64_t spuNumCycles; /** The number of cycles it takes the SPU unit to execute. */

	/***********************Brnach Predictor Parameters*************************//**
	*//****************************************************************************/
	Bp::Predictor_t branchPredictor; /** The type of branch predictor to use. */

	/****************************Memory Parameters*******************************//**
	*//****************************************************************************/
	uint64_t memorySize;                 /** The size of memory in words. */
	Arch::Address_t spStartingAddress;   /** The stack pointer starting address byte aligned. */
	Arch::Address_t textStartingAddress; /** The text memory starting address byte aligned. */
	/** The size of the cache in words must be power of two. Note: The size must be greater
	    than the product of the associtivity and number of blocks. */
	uint64_t cacheSize;
	uint64_t cacheAssocitivity;     /** The associtivity of the cache must be a power of two. */
	uint64_t cacheNumBlocks;        /** The number of blocks in a cache association. */
	uint64_t cacheMissCyclePenalty; /** Cahce miss penalty in cycles. */
} Config_t;

/** The current status of the datapath */
typedef enum Status {
	Status_Normal,
	Status_BreakPoint,
	Status_EndProgram,
} Status_t;

/***************************************************************************//**
	Class representing a 5 stage RISC datapath.
*//****************************************************************************/
class Datapath {
public:
	Datapath();
	Datapath(Config_t config);
	Config_t config;
	Arch::Address_t pc;      /** The program counter. */
	Reg::IntRegFile* iReg;   /** The integer register file. */
	Reg::FpRegFile* fReg;    /** The floating point register file. */
	Mem::Memory* mem;        /** The memory block. */
	Bp::BranchPredictor* bp; /** The branch predictor. */
	std::string getStr(void);
	void loadProgram(FILE* inputFile);
	Status_t stepCycle(void);
	Status_t stepCycles(uint64_t cycles);
	Status_t go(void);
	void outputDatapath(FILE* outputFile);
	void outputStatistics(FILE* outputFile);
	void outputIRegFile(FILE* outputFile);
	void outputFRegFile(FILE* outputFile);
	void outputDataMemory(FILE* outputFile);
private:
	Insts::Inst* IF_ID;  /** The output instruction of the IF stage feading the ID. */
	Insts::Inst* ID_EX;  /** The output instruction of the ID stage feading the EX. */
	Insts::Inst* EX_MEM; /** The output instruction of the EX stage feading the MEM. */
	Insts::Inst* MEM_WB; /** The output instruction of the MEM stage feading the WB. */
	Insts::Inst* WB_END; /** The output instruction of the WB stage feading the END. */
	Insts::Nop* null(void);
	Insts::Inst* fetch(void);
	void stepNormal(void);
	void stepStallInID(void);
	void stepStallInEX(void);
	void stepStallInMEM(void);
	void stepPcChangeInIF(void);
	void stepPcChangeInID(void);
	void stepPcChangeInEX(void);
	bool shouldStallInID(void);
	bool shouldStallInEX(void);
	bool shouldStallInMEM(void);
	bool shouldPcChangeInIF(void);
	bool shouldPcChangeInID(void);
	bool shouldPcChangeInEX(void);
	/** The number of retired instructions of each instruction type. */
	uint64_t retiredInstructions[NUM_INSTRUCTIONS] = {0};
	uint64_t cycles;       /** The number of executed cycles. */
	uint64_t cacheMisses;  /** The number of cache misses. */
	uint64_t cacheHits;    /** The number of cache hits. */
	void analyze(void);
};

extern Config_t defaultConfig;

}

#endif /* __DATAPATH_HPP__ */
