/***************************************************************************//**
    \file BranchPredictor.cpp
    \brief Branch predictor interface.
*//****************************************************************************/
#include "BranchPredictor.hpp"
#include "BaseInstruction.hpp"
#include "Instructions.hpp"

namespace Bp {

/***************************************************************************//**
    Branch predictor constructor.
*//****************************************************************************/
BranchPredictor::BranchPredictor(Predictor_t predictor)
    : predictor(predictor),
      correctTaken(0),
      predictedTaken(0),
      actualTaken(0),
      correctNotTaken(0),
      predictedNotTaken(0),
      actualNotTaken(0),
      globalHistory(TwoBitCounter_TakenWeakly) {
}

/***************************************************************************//**
    Make a prediction.

    @param inst - The instructin to generate a prediction for.
*//****************************************************************************/
Prediction_t BranchPredictor::predict(Insts::Inst* inst) {
    if(Insts::BInst* bInst = dynamic_cast<Insts::BInst*>(inst)) {
        switch (predictor) {
            case Predictor_None:
                return Prediction_None;
            case Predictor_AlwaysTaken:
                return Prediction_Taken;
            case Predictor_AlwaysNotTaken:
                return Prediction_NotTaken;
            case Predictor_ForwardTaken:
                return (Util::signExtend(bInst->getIm()) > 0) ? Prediction_Taken : Prediction_NotTaken;
            case Predictor_BackwardTaken:
                return (Util::signExtend(bInst->getIm()) < 0) ? Prediction_Taken : Prediction_NotTaken;
            case Predictor_Random:
                return (Prediction_t) ((rand() % 2) + 1);
            case Predictor_Local:
                return localPredict(inst->getPc());
            case Predictor_Global:
                return globalPredict();
            default:
                assert(false && "Unsupported branch predictor.");
        }
    } else {
        return Prediction_None;
    }
}

/***************************************************************************//**
    Update stats based on a prediction and the actual result.

    @param inst - The insturction to update the predictor with.
    @param actual - The actual result.
*//****************************************************************************/
void BranchPredictor::update(Insts::Inst* inst) {
    Prediction_t predicted = inst->getPredictedBranch();
    Prediction_t actual = inst->getActualBranch();
    switch (predictor) {
        case Predictor_None:
        case Predictor_AlwaysTaken:
        case Predictor_AlwaysNotTaken:
        case Predictor_ForwardTaken:
        case Predictor_BackwardTaken:
        case Predictor_Random:
            break;
        case Predictor_Local:
            localUpdate(inst->getPc(), actual);
            break;
        case Predictor_Global:
            globalUpdate(actual);
            break;
        default:
            assert(false && "Unsupported branch predictor.");
    }
    switch(predicted) {
        case Prediction_None:
            break;
        case Prediction_Taken:
            predictedTaken++;
            if (actual == Prediction_Taken) {
                correctTaken++;
            }
            break;
        case Prediction_NotTaken:
            predictedNotTaken++;
            if (actual == Prediction_NotTaken) {
                correctNotTaken++;
            }
            break;
        default:
            assert(false && "Unsupported branch prediction.");
    }
    switch(actual) {
        case Prediction_None:
            break;
        case Prediction_Taken:
            actualTaken++;
            break;
        case Prediction_NotTaken:
            actualNotTaken++;
            break;
        default:
            assert(false && "Unsupported branch prediction.");
    }
}

/***************************************************************************//**
    Get the string representation of a branch prediction

    @param prediciton - The prediction.
    @return The string.
*//****************************************************************************/
std::string BranchPredictor::getPredictionStr(Prediction_t prediction) {
    switch (prediction) {
        case Prediction_None: return "None";
        case Prediction_Taken: return "Taken";
        case Prediction_NotTaken: return "NotTaken";
        default: assert(false && "Missing branch prediction.");
    }
}

/***************************************************************************//**
    Make a prediction using a local history table.

    @param pc - The pc of the instruction to make a prediction for.
*//****************************************************************************/
Prediction_t BranchPredictor::localPredict(Arch::Address_t pc) {
    /* Check if the pc has not been added to the local history */
    if (localHistory.find(pc) == localHistory.end()) {
        /* Default to taken weakly */
        localHistory[pc] = TwoBitCounter_TakenWeakly;
    }
    return predictionFromTwoBitCounter(localHistory[pc]);
}

/***************************************************************************//**
    Make a prediction using the global history.
*//****************************************************************************/
Prediction_t BranchPredictor::globalPredict(void) {
    return predictionFromTwoBitCounter(globalHistory);
}

/***************************************************************************//**
    Update the local history table.

    @param pc - The pc to update.
    @param actual - The actual branch.
*//****************************************************************************/
void BranchPredictor::localUpdate(Arch::Address_t pc, Prediction_t actual) {
    localHistory[pc] = updateTwoBitCounter(localHistory[pc], actual);
}

/***************************************************************************//**
    Update the global history table.

    @param actual - The actual branch.
*//****************************************************************************/
void BranchPredictor::globalUpdate(Prediction_t actual) {
    globalHistory = updateTwoBitCounter(globalHistory, actual);
}

/***************************************************************************//**
    Convert a two bit counter value to a prediction.

    @param counter - The two bit counter value.
*//****************************************************************************/
Prediction_t BranchPredictor::predictionFromTwoBitCounter(TwoBitCounter_t counter) {
    switch(counter) {
        case TwoBitCounter_NotTakenStrongly:
            return Prediction_NotTaken;
        case TwoBitCounter_NotTakenWeakly:
            return Prediction_NotTaken;
        case TwoBitCounter_TakenWeakly:
            return Prediction_Taken;
        case TwoBitCounter_TakenStrongly:
            return Prediction_Taken;
        default:
            assert(false && "Unsupported counter value.");
    }
}

/***************************************************************************//**
    Get the next value of a two bit counter.

    @param counter - The two bit counter value.
    @param prediction - The prediction to update the value for.
*//****************************************************************************/
TwoBitCounter_t BranchPredictor::updateTwoBitCounter(TwoBitCounter_t counter, Prediction_t prediction) {
    switch (prediction) {
        case Prediction_None:
            return counter;
        case Prediction_Taken:
            switch(counter) {
                case TwoBitCounter_NotTakenStrongly:
                    return TwoBitCounter_NotTakenWeakly;
                case TwoBitCounter_NotTakenWeakly:
                    return TwoBitCounter_TakenWeakly;
                case TwoBitCounter_TakenWeakly:
                    return TwoBitCounter_TakenStrongly;
                case TwoBitCounter_TakenStrongly:
                    return TwoBitCounter_TakenStrongly;
                default:
                    assert(false && "Unsupported counter value.");
            }
        case Prediction_NotTaken:
            switch(counter) {
                case TwoBitCounter_NotTakenStrongly:
                    return TwoBitCounter_NotTakenStrongly;
                case TwoBitCounter_NotTakenWeakly:
                    return TwoBitCounter_NotTakenStrongly;
                case TwoBitCounter_TakenWeakly:
                    return TwoBitCounter_NotTakenWeakly;
                case TwoBitCounter_TakenStrongly:
                    return TwoBitCounter_TakenWeakly;
                default:
                    assert(false && "Unsupported counter value.");
            }
        default:
            assert(false && "Missing branch prediction.");
    }
}

}
