/***************************************************************************//**
    \file Instructions.hpp
    \brief Header for the interface to the instructions that comprise the core ISA.
*//****************************************************************************/
#ifndef __INSTRUCTIONS_HPP__
#define __INSTRUCTIONS_HPP__

#include "Util.hpp"

#include "ExecutionUnit.hpp"
#include "RegisterFile.hpp"
#include "Memory.hpp"
#include "BaseInstruction.hpp"
#include "SpecialInstructions.hpp"

namespace Datapath {
	class Datapath;
}

namespace Insts {

/***************************************************************************//**
	R type instructions.
*//****************************************************************************/
class RInst : public Inst {
public:
	RInst(Datapath::Datapath* datapath,
		  Arch::Address_t pc,
		  Arch::UnsignedWord_t rawInstruction,
		  Arch::Op_t opcode,
		  Eu::Op_t euOp);
	virtual ~RInst(){};
	Arch::Reg_t getRd(void){ return rd; };
	Arch::Reg_t getRs(void){ return rs; };
	Arch::Reg_t getRt(void){ return rt; };
protected:
	Arch::Reg_t rd; /** rd register */
	Arch::Reg_t rs; /** rs register */
	Arch::Reg_t rt; /** rt register */
	void setEuOperands(void);
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
	std::string getInstructionStr(void);
};

class Nop : public RInst {
public:
	Nop(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_nop, Eu::Op_Add){};
	~Nop(){};
};

class Brk : public RInst {
public:
	Brk(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_brk, Eu::Op_Add){};
	~Brk(){};
};

class Add : public RInst {
public:
	Add(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_add, Eu::Op_Add){};
	~Add(){};
};

class Sub : public RInst {
public:
	Sub(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_sub, Eu::Op_Subtract){};
	~Sub(){};
};

class Mul : public RInst {
public:
	Mul(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_mul, Eu::Op_Multiply){};
	~Mul(){};
};

class Div : public RInst {
public:
	Div(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_div, Eu::Op_Divide){};
	~Div(){};
};

class And : public RInst {
public:
	And(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_and, Eu::Op_And){};
	~And(){};
};

class Or : public RInst {
public:
	Or(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_or, Eu::Op_Or){};
	~Or(){};
};

class Xor : public RInst {
public:
	Xor(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_xor, Eu::Op_Xor){};
	~Xor(){};
};

class Shl : public RInst {
public:
	Shl(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_shl, Eu::Op_ShiftLeft){};
	~Shl(){};
};

class Shr : public RInst {
public:
	Shr(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_shr, Eu::Op_ShiftRight){};
	~Shr(){};
};

class Slt : public RInst {
public:
	Slt(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_slt, Eu::Op_Subtract){};
	~Slt(){};
protected:
	void setWbData(void){ wbData = Util::unsignedWord(euResult.negative); }
};

class Jr : public RInst {
public:
	Jr(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_jr, Eu::Op_Add){};
	~Jr(){};
protected:
	Arch::Address_t getNextPcId(void);
	Arch::Address_t getNextPcEx(void){ return euOperandA.u; }
};

class Jrl : public RInst {
public:
	Jrl(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_jrl, Eu::Op_Add){ wbReg = Arch::Reg_ra; };
	~Jrl(){};
protected:
	void setWbData(void) { wbData = Util::unsignedWord(getNextPcDefault()); };
	Arch::Address_t getNextPcId(void);
	Arch::Address_t getNextPcEx(void){ return euOperandA.u; }
};

class Halt : public RInst {
public:
	Halt(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: RInst(datapath, pc, rawInstruction, Arch::Op_halt, Eu::Op_Add){};
	~Halt(){};
};

/***************************************************************************//**
	I type instructions.
*//****************************************************************************/
class IInst : public Inst {
public:
	IInst(Datapath::Datapath* datapath,
		  Arch::Address_t pc,
		  Arch::UnsignedWord_t rawInstruction,
		  Arch::Op_t opcode,
		  Eu::Op_t euOp);
	virtual ~IInst(){};
	Arch::Reg_t getRd() { return rd; };
	Arch::Reg_t getRs() { return rs; };
	Arch::Immediate_t getIm() { return im; };
protected:
	Arch::Reg_t rd;       /** rd register */
	Arch::Reg_t rs;       /** rs register */
	Arch::Immediate_t im; /** immediate */
	void setEuOperands(void);
	void setDependentRegs(void) { dependentRegs = {rs}; }
	std::string getInstructionStr(void);
};

class Addi : public IInst {
public:
	Addi(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: IInst(datapath, pc, rawInstruction, Arch::Op_addi, Eu::Op_Add){};
	~Addi(){};
};

class Andi : public IInst {
public:
	Andi(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: IInst(datapath, pc, rawInstruction, Arch::Op_andi, Eu::Op_And){};
	~Andi(){};
};

class Ori : public IInst {
public:
	Ori(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: IInst(datapath, pc, rawInstruction, Arch::Op_ori, Eu::Op_Or){};
	~Ori(){};
};

class Slti : public IInst {
public:
	Slti(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: IInst(datapath, pc, rawInstruction, Arch::Op_slti, Eu::Op_Subtract){};
	~Slti(){};
protected:
	void setWbData(void){ wbData = Util::unsignedWord(euResult.negative); }
};

class Loi : public IInst {
public:
	Loi(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: IInst(datapath, pc, rawInstruction, Arch::Op_loi, Eu::Op_And){};
	~Loi(){};
protected:
	void setEuOperands(void) {
		euOperandA = Util::unsignedWord(0x0000ffff);
		euOperandB = Util::unsignedWord(im);
	}
};

class Hii : public IInst {
public:
	Hii(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: IInst(datapath, pc, rawInstruction, Arch::Op_hii, Eu::Op_Or){};
	~Hii(){};
protected:
	void setDependentRegs(void) { dependentRegs = {rd}; }
	void setEuOperands(void);
};

/***************************************************************************//**
	M type instructions.
*//****************************************************************************/
class MInst : public Inst {
public:
	MInst(Datapath::Datapath* datapath,
		  Arch::Address_t pc,
		  Arch::UnsignedWord_t rawInstruction,
		  Arch::Op_t opcode,
		  Mem::Mode_t memMode,
		  Arch::Reg_t wbReg);
	virtual ~MInst(){};
	Arch::Reg_t getRd() { return rd; };
	Arch::Reg_t getRs() { return rs; };
	Arch::Immediate_t getIm() { return im; };
protected:
	Arch::Reg_t rd;       /** rd register */
	Arch::Reg_t rs;       /** rs register */
	Arch::Immediate_t im; /** immediate */
	void setEuOperands(void);
	std::string getInstructionStr(void);
};

class Lw : public MInst {
public:
	Lw(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: MInst(datapath, pc, rawInstruction, Arch::Op_lw, Mem::Mode_Read, Util::getReg1(rawInstruction)){};
	~Lw(){};
protected:
	void setDependentRegs(void) { dependentRegs = {rs}; }
	void setWbData(void){ wbData = memTransaction.data; }
};

class Sw : public MInst {
public:
	Sw(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: MInst(datapath, pc, rawInstruction, Arch::Op_sw, Mem::Mode_Write, Arch::Reg_r0){};
	~Sw(){};
protected:
	void setDependentRegs(void) { dependentRegs = {rd, rs}; }
	void setMemWriteData(void);
};

/***************************************************************************//**
	B type instructions.
*//****************************************************************************/
class BInst : public Inst {
public:
	BInst(Datapath::Datapath* datapath,
		  Arch::Address_t pc,
		  Arch::UnsignedWord_t rawInstruction,
		  Arch::Op_t opcode);
	virtual ~BInst(){};
	Arch::Reg_t getRs() { return rs; };
	Arch::Reg_t getRt() { return rt; };
	Arch::Immediate_t getIm() { return im; };
protected:
	Arch::Reg_t rs;       /** rs register */
	Arch::Reg_t rt;       /** rt register */
	Arch::Immediate_t im; /** immediate */
	Arch::Address_t getNextPcIf(void);
	Arch::Address_t getNextPcId(void);
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
	void setEuOperands(void);
	std::string getInstructionStr(void);
};

class Beq : public BInst {
public:
	Beq(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: BInst(datapath, pc, rawInstruction, Arch::Op_beq){ nextPc = getNextPcIf(); };
	~Beq(){};
protected:
	void setActualBranch(void) {actualBranch = euResult.zero ? Bp::Prediction_Taken : Bp::Prediction_NotTaken; };
	Arch::Address_t getNextPcEx(void) { return euResult.zero ? (pc + Util::signExtend(im)) : getNextPcDefault(); };
};

class Bne : public BInst {
public:
	Bne(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: BInst(datapath, pc, rawInstruction, Arch::Op_bne){ nextPc = getNextPcIf(); };
	~Bne(){};
protected:
	void setActualBranch(void) {actualBranch = !euResult.zero ? Bp::Prediction_Taken : Bp::Prediction_NotTaken; };
	Arch::Address_t getNextPcEx(void) { return !euResult.zero ? (pc + Util::signExtend(im)) : getNextPcDefault(); };
};

/***************************************************************************//**
	J type instructions.
*//****************************************************************************/
class JInst : public Inst {
public:
	JInst(Datapath::Datapath* datapath,
		  Arch::Address_t pc,
		  Arch::UnsignedWord_t rawInstruction,
		  Arch::Op_t opcode,
		  Arch::Reg_t wbReg);
	virtual ~JInst(){};
	Arch::Immediate_t getAddr() { return addr; };
protected:
	Arch::Address_t addr; /** address */
	Arch::Address_t getNextPcIf(void);
	Arch::Address_t getNextPcId(void);
	Arch::Address_t getNextPcEx(void) { return addr; };
	std::string getInstructionStr(void);
};

class J : public JInst {
public:
	J(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: JInst(datapath, pc, rawInstruction, Arch::Op_j, Arch::Reg_r0){ nextPc = getNextPcIf(); };
	~J(){};
};

class Jal : public JInst {
public:
	Jal(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: JInst(datapath, pc, rawInstruction, Arch::Op_jal, Arch::Reg_ra){ nextPc = getNextPcIf(); };
	~Jal(){};
protected:
	void setWbData(void) { wbData = Util::unsignedWord(getNextPcDefault()); };
};

/***************************************************************************//**
	FR type instructions.
*//****************************************************************************/
class FRInst : public Inst {
public:
	FRInst(Datapath::Datapath* datapath,
		   Arch::Address_t pc,
		   Arch::UnsignedWord_t rawInstruction,
		   Arch::Op_t opcode,
		   Eu::Op_t euOp);
	virtual ~FRInst(){};
	Arch::Reg_t getRd(void){ return rd; };
    Arch::Reg_t getRs(void){ return rs; };
    Arch::Reg_t getRt(void){ return rt; };
protected:
	Arch::Reg_t rd; /** rd register */
	Arch::Reg_t rs; /** rs register */
	Arch::Reg_t rt; /** rt register */
	void setEuOperands(void);
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
	std::string getInstructionStr(void);
};

class Addf : public FRInst {
public:
	Addf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FRInst(datapath, pc, rawInstruction, Arch::Op_addf, Eu::Op_Add){};
	~Addf(){};
};

class Subf : public FRInst {
public:
	Subf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FRInst(datapath, pc, rawInstruction, Arch::Op_subf, Eu::Op_Subtract){};
	~Subf(){};
};

class Mulf : public FRInst {
public:
	Mulf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FRInst(datapath, pc, rawInstruction, Arch::Op_mulf, Eu::Op_Multiply){};
	~Mulf(){};
};

class Divf : public FRInst {
public:
	Divf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FRInst(datapath, pc, rawInstruction, Arch::Op_divf, Eu::Op_Divide){};
	~Divf(){};
};

class Sltf : public FRInst {
public:
	Sltf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FRInst(datapath, pc, rawInstruction, Arch::Op_sltf, Eu::Op_Subtract){};
	~Sltf(){};
protected:
	void setWbData(void){ wbData = Util::floatWord(euResult.negative); }
};

/***************************************************************************//**
	FI type instructions.
*//****************************************************************************/
class FIInst : public Inst {
public:
	FIInst(Datapath::Datapath* datapath,
		   Arch::Address_t pc,
		   Arch::UnsignedWord_t rawInstruction,
		   Arch::Op_t opcode,
		   Eu::Op_t euOp);
	virtual ~FIInst(){};
	Arch::Reg_t getRd() { return rd; };
	Arch::Reg_t getRs() { return rs; };
	Arch::Immediate_t getIm() { return im; };
protected:
	Arch::Reg_t rd;       /** rd register */
	Arch::Reg_t rs;       /** rs register */
	Arch::Immediate_t im; /** immediate */
	std::string getInstructionStr(void);
};

class Lof : public FIInst {
public:
	Lof(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FIInst(datapath, pc, rawInstruction, Arch::Op_lof, Eu::Op_And){};
	~Lof(){};
protected:
	void setEuOperands(void) {
		euOperandA = Util::unsignedWord(0x0000ffff);
		euOperandB = Util::unsignedWord(im);
	}
};

class Hif : public FIInst {
public:
	Hif(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FIInst(datapath, pc, rawInstruction, Arch::Op_hif, Eu::Op_Or){};
	~Hif(){};
protected:
	void setDependentRegs(void) { dependentRegs = {rd}; }
	void setEuOperands(void);
};

/***************************************************************************//**
	FM type instructions.
*//****************************************************************************/
class FMInst : public Inst {
public:
	FMInst(Datapath::Datapath* datapath,
		   Arch::Address_t pc,
		   Arch::UnsignedWord_t rawInstruction,
		   Arch::Op_t opcode,
		   Mem::Mode_t memMode,
		   Arch::Reg_t wbReg);
	virtual ~FMInst(){};
	Arch::Reg_t getRd() { return rd; };
	Arch::Reg_t getRs() { return rs; };
	Arch::Immediate_t getIm() { return im; };
protected:
	Arch::Reg_t rd;       /** rd register */
	Arch::Reg_t rs;       /** rs register */
	Arch::Immediate_t im; /** immediate */
	void setEuOperands(void);
	std::string getInstructionStr(void);
};

class Lwf : public FMInst {
public:
	Lwf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FMInst(datapath, pc, rawInstruction, Arch::Op_lwf, Mem::Mode_Read, Util::getReg1(rawInstruction)){};
	~Lwf(){};
protected:
	void setDependentRegs(void) { dependentRegs = {rs}; }
	void setWbData(void){ wbData = memTransaction.data; }
};

class Swf : public FMInst {
public:
	Swf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FMInst(datapath, pc, rawInstruction, Arch::Op_swf, Mem::Mode_Write, Arch::Reg_fr0){};
	~Swf(){};
protected:
	void setDependentRegs(void) { dependentRegs = {rd, rs}; }
	void setMemWriteData(void);
};

/***************************************************************************//**
	FB type instructions.
*//****************************************************************************/
class FBInst : public Inst {
public:
	FBInst(Datapath::Datapath* datapath,
		   Arch::Address_t pc,
		   Arch::UnsignedWord_t rawInstruction,
		  Arch::Op_t opcode);
	virtual ~FBInst(){};
	Arch::Reg_t getRs() { return rs; };
	Arch::Reg_t getRt() { return rt; };
	Arch::Immediate_t getIm() { return im; };
protected:
	Arch::Reg_t rs;       /** rs register */
	Arch::Reg_t rt;       /** rt register */
	Arch::Immediate_t im; /** immediate */
	Arch::Address_t getNextPcIf(void);
	Arch::Address_t getNextPcId(void);
	void setDependentRegs(void) { dependentRegs = {rs, rt}; }
	void setEuOperands(void);
	std::string getInstructionStr(void);
};

class Beqf : public FBInst {
public:
	Beqf(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FBInst(datapath, pc, rawInstruction, Arch::Op_beqf){ nextPc = getNextPcIf(); };
	~Beqf(){};
protected:
	void setActualBranch(void) {actualBranch = euResult.zero ? Bp::Prediction_Taken : Bp::Prediction_NotTaken; };
	Arch::Address_t getNextPcEx(void) { return euResult.zero ? (pc + Util::signExtend(im)) : getNextPcDefault(); };
};

class Bnef : public FBInst {
public:
	Bnef(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction)
		: FBInst(datapath, pc, rawInstruction, Arch::Op_bnef){ nextPc = getNextPcIf(); };
	~Bnef(){};
protected:
	void setActualBranch(void) {actualBranch = !euResult.zero ? Bp::Prediction_Taken : Bp::Prediction_NotTaken; };
	Arch::Address_t getNextPcEx(void) { return !euResult.zero ? (pc + Util::signExtend(im)) : getNextPcDefault(); };
};

extern Inst* IInstFactoryMethod(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction);

}

#endif /*  pc, rawInstruction,__INSTRUCTIONS_HPP__ */
