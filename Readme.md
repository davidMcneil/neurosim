Neurosim
==============

Building
--------------
- **make build** - builds the main executable
- **make buildtest** - builds the automated test suite
- **make all** - builds the main executable and the automated test suite
- **make run** - run the main executable with the parameters specified in the Makefile
- **make test** - run the automated test suite

Clean Up
--------------
- **make clean** - remove object files
- **make rm** - remove object files, executable files, and simulator output files

Running
--------------
- Options
	* **-s** - output a statistics file to statistics.txt
	* **-r** - output the final register file to registers.txt
	* **-t** - output a cycle accurate trace to trace.txt
	* **-i** - run the simulator in interactive mode
	* **-f** - the input binary file
- Example
	* ./bin/neurosim -srt -f samples/intercept.bin

Interactive Mode
--------------
Interactive mode is initiated with the -i option.
When the simulator starts the following commands can be used:

- **c** - step by a single cycle
- **g** - run until the end of program
- **r** - perform an integer register dump
- **f** - perform a floating point register dump
- **d** - exit interactive mode, this is when statistics, register, and trace files will be written
