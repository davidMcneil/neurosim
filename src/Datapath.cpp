/***************************************************************************//**
    \file Datapath.hpp
    \brief The RISC style datapath.
*//****************************************************************************/

#include "Datapath.hpp"

/** The size of memory in words. */
#define MEMORY_SIZE                     (0x10000)

namespace Datapath {

/** The default datapath configuration. */
Config_t defaultConfig = {
    .controlChangeDetectionInFetch = true,
    .controlChangeDetectionInDecode = true,
    .fpuNumCycles = 3,
    .spuNumCycles = 5,
    .branchPredictor = Bp::Predictor_AlwaysTaken,
    .memorySize = MEMORY_SIZE,
    .spStartingAddress = Mem::Memory::word2byteAligned(MEMORY_SIZE - 1),
    .textStartingAddress = Mem::Memory::word2byteAligned(0),
    .cacheSize = 32,
    .cacheAssocitivity = 4,
    .cacheNumBlocks = 4,
    .cacheMissCyclePenalty = 5,
};

/***************************************************************************//**
    Datapath constructor.
*//****************************************************************************/
Datapath::Datapath()
    : Datapath(defaultConfig) {
}

/***************************************************************************//**
    Datapath constructor.

    @param config - The datapath configuration.
*//****************************************************************************/
Datapath::Datapath(Config_t config)
    : config(config),
      pc(config.textStartingAddress),
      iReg(new Reg::IntRegFile(NUM_INT_REGISTERS, config.spStartingAddress)),
      fReg(new Reg::FpRegFile(NUM_FP_REGISTERS)),
      mem(new Mem::Memory(config.memorySize, config.textStartingAddress, config.spStartingAddress,
                          config.cacheSize, config.cacheAssocitivity, config.cacheNumBlocks)),
      bp(new Bp::BranchPredictor(config.branchPredictor)),
      IF_ID(null()),
      ID_EX(null()),
      EX_MEM(null()),
      MEM_WB(null()),
      WB_END(null()),
      cycles(0),
      cacheMisses(0),
      cacheHits(0) {
}

/***************************************************************************//**
    Load a binary input file into the datapaths instruction memory
*//****************************************************************************/
void Datapath::loadProgram(FILE* inputFile) {
    mem->loadTextAndData(inputFile);
}

/***************************************************************************//**
    Step the datapath one cycle.

    @return The datapath status after the step.
*//****************************************************************************/
Status_t Datapath::stepCycle(void) {
    if (WB_END->getOpcode() == Arch::Op_halt) {
        return Status_EndProgram;
    }
    else {
        if(shouldStallInMEM()) {
            stepStallInMEM();
        }
        else if(shouldStallInEX()) {
            stepStallInEX();
        }
        /* A PC change in EX is always a definite. */
        else if (shouldPcChangeInEX()) {
            stepPcChangeInEX();
        }
        else if (shouldStallInID()) {
            stepStallInID();
        }
        /* A PC change in ID could be due to branch prediction. */
        else if (shouldPcChangeInID()) {
            stepPcChangeInID();
        }
        /* A PC change in IF could be due to branch prediction. */
        else if(shouldPcChangeInIF()) {
            stepPcChangeInIF();
        }
        else {
            stepNormal();
        }
        analyze();
        if (IF_ID->getOpcode() == Arch::Op_brk) {
            return Status_BreakPoint;
        } else {
            return Status_Normal;
        }
    }
}

/***************************************************************************//**
    Step the datapath the indicated number of cycles. Stop if a non normal status
    is encountered.

    @return The datapath status after the steps.
*//****************************************************************************/
Status_t Datapath::stepCycles(uint64_t cycles) {
    Status_t status = Status_Normal;
    uint64_t i;
    for(i = 0; i < cycles; i++) {
        status = stepCycle();
        if (status != Status_Normal) {
            return status;
        }
    }
    return Status_Normal;
}

/***************************************************************************//**
    Step the datapath until a non normal status is reached.

    @return The datapath status after running.
*//****************************************************************************/
Status_t Datapath::go(void) {
    Status_t status = Status_Normal;
    do {
        status = stepCycle();
    } while ( status == Status_Normal );
    return status;
}

/***************************************************************************//**
    Generate a null instruction. This is used when flushing pipeline stages.
*//****************************************************************************/
Insts::Nop* Datapath::null(void){
    return new Insts::Nop(this, mem->getTextAddressEnd(), 0x00000000);
}

/***************************************************************************//**
    If the pc is in the instruction segment of memory fetch the instruction
    at the current pc. Otherwise return a nop instruction. This prevents the
    datapath from fetching from the data portion of memory after the halt
    instruction has been decoded.

    @return The instruction.
*//****************************************************************************/
Insts::Inst* Datapath::fetch(void) {
    if (mem->textAddress(pc)) {
        return Insts::Inst::IF(this);
    } else {
        return null();
    }
}

/***************************************************************************//**
    Perform a normal step through the pipeline stages.
*//****************************************************************************/
void Datapath::stepNormal(void) {
    delete WB_END;
    WB_END = MEM_WB->WB();
    MEM_WB = EX_MEM->MEM();
    EX_MEM = ID_EX->EX();
    ID_EX = IF_ID->ID();
    IF_ID = fetch();
    pc = IF_ID->getNextPc();
}

/***************************************************************************//**
    Step the pipeline with a stall in the decode stage.
*//****************************************************************************/
void Datapath::stepStallInID(void) {
    delete WB_END;
    WB_END = MEM_WB->WB();
    MEM_WB = EX_MEM->MEM();
    EX_MEM = null();
    ID_EX = ID_EX->ID();
    /* IF_ID has already been fetched. Do not increment PC. */
}

/***************************************************************************//**
    Step the pipeline with a stall in the execute stage.
*//****************************************************************************/
void Datapath::stepStallInEX(void) {
    delete WB_END;
    WB_END = MEM_WB->WB();
    MEM_WB = null();
    EX_MEM = EX_MEM->EX();
    ID_EX = ID_EX->ID();
    /* IF_ID has already been fetched. Do not increment PC. */
}

/***************************************************************************//**
    Step the pipeline with a stall in the memory stage.
*//****************************************************************************/
void Datapath::stepStallInMEM(void) {
    delete WB_END;
    WB_END = null();
    MEM_WB = MEM_WB->MEM();
    if (EX_MEM->getExCycles() > 0) {
        /* Only execute if there are cycles left. */
        EX_MEM = EX_MEM->EX();
    }
    ID_EX = ID_EX->ID();
    /* IF_ID has already been fetched. Do not increment PC. */
}

/***************************************************************************//**
    Step with a PC change in the fetch stage.
*//****************************************************************************/
void Datapath::stepPcChangeInIF(void) {
    pc = IF_ID->getNextPc(); /** Update new PC. */
    delete WB_END;
    WB_END = MEM_WB->WB();
    MEM_WB = EX_MEM->MEM();
    EX_MEM = ID_EX->EX();
    ID_EX = IF_ID->ID();
    IF_ID = fetch();
    pc = IF_ID->getNextPc(); /** Increment new PC. */
}

/***************************************************************************//**
    Step with a PC change in the decode stage.
*//****************************************************************************/
void Datapath::stepPcChangeInID(void) {
    pc = ID_EX->getNextPc(); /** Update new PC. */
    delete WB_END;
    WB_END = MEM_WB->WB();
    MEM_WB = EX_MEM->MEM();
    EX_MEM = ID_EX->EX();
    ID_EX = null();
    delete IF_ID;
    IF_ID = fetch();
    pc = IF_ID->getNextPc(); /** Increment new PC. */
}

/***************************************************************************//**
    Step with a PC change in the execute stage.
*//****************************************************************************/
void Datapath::stepPcChangeInEX(void) {
    pc = EX_MEM->getNextPc(); /** Update new PC. */
    delete WB_END;
    WB_END = MEM_WB->WB();
    MEM_WB = EX_MEM->MEM();
    EX_MEM = null();
    delete ID_EX;
    ID_EX = null();
    delete IF_ID;
    IF_ID = fetch();
    pc = IF_ID->getNextPc(); /** Increment new PC. */
}

/***************************************************************************//**
    Detect a stall in the decode stage.
*//****************************************************************************/
bool Datapath::shouldStallInID(void) {
    return (ID_EX->dependsOnReg(EX_MEM->getWbReg()) ||
            ID_EX->dependsOnReg(MEM_WB->getWbReg()));
}

/***************************************************************************//**
    Detect a stall in the execute stage.
*//****************************************************************************/
bool Datapath::shouldStallInEX(void) {
    Arch::Op_t exOpcode = EX_MEM->getOpcode();
    return (EX_MEM->getExCycles() > 0 && exOpcode != Arch::Op_nop);
}

/***************************************************************************//**
    Detect a stall in the memory stage.
*//****************************************************************************/
bool Datapath::shouldStallInMEM(void) {
    Arch::Op_t memOpcode = MEM_WB->getOpcode();
    return (MEM_WB->getMemCycles() > 0 && memOpcode != Arch::Op_nop);
}

/***************************************************************************//**
    Detect a PC change in the decode stage.Detect a stall in the fetch stage.
*//****************************************************************************/
bool Datapath::shouldPcChangeInIF(void) {
    /** We cannot update in fetch if there is an instruction that
        could potentially update in execute. */
    Arch::InstructionType_t idType = ID_EX->getType();
    Arch::Op_t idOpcode = ID_EX->getOpcode();
    if (IF_ID->controlChangeInIf() &&
        idOpcode != Arch::Op_jr &&
        idOpcode != Arch::Op_jrl &&
        idType != Arch::InstructionType_B &&
        idType != Arch::InstructionType_FB) {
        return true;
    } else {
        return false;
    }
}

/***************************************************************************//**
    Detect a PC change in the decode stage.
*//****************************************************************************/
bool Datapath::shouldPcChangeInID(void) {
    if (ID_EX->controlChangeInId()) {
        return true;
    } else {
        return false;
    }
}

/***************************************************************************//**
    Detect a PC change in the execute stage.
*//****************************************************************************/
bool Datapath::shouldPcChangeInEX(void) {
    if (EX_MEM->controlChangeInEx()) {
        return true;
    } else {
        return false;
    }
}

/***************************************************************************//**
    Get the string representation of the datapath.

    @return The datapaths string representation.
*//****************************************************************************/
std::string Datapath::getStr(void) {
    std::stringstream stream;
    std::istringstream IF_IDstr(IF_ID->getStr());
    std::istringstream ID_EXstr(ID_EX->getStr());
    std::istringstream EX_MEMstr(EX_MEM->getStr());
    std::istringstream MEM_WBstr(MEM_WB->getStr());
    std::istringstream WB_ENDstr(WB_END->getStr());
    std::string IF_IDline;
    std::string ID_EXline;
    std::string EX_MEMline;
    std::string MEM_WBline;
    std::string WB_ENDline;
    stream << std::left << std::setw(27) << "IF_ID";
    stream << std::left << std::setw(27) << "ID_EX";
    stream << std::left << std::setw(27) << "EX_MEM";
    stream << std::left << std::setw(27) << "MEM_WB";
    stream << std::left << std::setw(27) << "WB_END";
    stream << "\n";
    while (std::getline(IF_IDstr, IF_IDline) &&
           std::getline(ID_EXstr, ID_EXline) &&
           std::getline(EX_MEMstr, EX_MEMline) &&
           std::getline(MEM_WBstr, MEM_WBline) &&
           std::getline(WB_ENDstr, WB_ENDline)) {
        stream << IF_IDline << ID_EXline << EX_MEMline << MEM_WBline << WB_ENDline << "\n";
    }
    return stream.str();
}

/***************************************************************************//**
    Analyze the current state of the datapath and update the statistics. This
    should only be called in stepCycle.
*//****************************************************************************/
void Datapath::analyze(void) {
    cycles++;
    retiredInstructions[(uint8_t) WB_END->getOpcode()]++;
    if (WB_END->getMemTransaction().status == Mem::CacheStatus_Miss) {
        cacheMisses++;
    } else if (WB_END->getMemTransaction().status == Mem::CacheStatus_Hit) {
        cacheHits++;
    }
}

/***************************************************************************//**
    Print out the current datapath.

    @param outputFile - The outputfile to print to.
*//****************************************************************************/
void Datapath::outputDatapath(FILE* outputFile) {
    fprintf(outputFile, "%s", getStr().c_str());
}

/***************************************************************************//**
    Print out the current datapath statistics.

    @param outputFile - The outputfile to print to.
*//****************************************************************************/
void Datapath::outputStatistics(FILE* outputFile) {
    uint64_t i;
    uint64_t totalInstructions = 0;
    /* Ouput instruction counts.*/
    for (i = 0; i < NUM_INSTRUCTIONS; i++) {
        if ((Arch::Op_t) i != Arch::Op_nop) {
            totalInstructions = totalInstructions + retiredInstructions[i];
        }
        fprintf(outputFile, "%s : %lu\n", Insts::Inst::lookupOpcodeStr((Arch::Op_t) i).c_str(), retiredInstructions[i]);
    }
    fprintf(outputFile, "TotalNonNopInstructions : %lu\n", totalInstructions);
    fprintf(outputFile, "Cycles : %lu\n", cycles);
    fprintf(outputFile, "IPC : %f\n", ((double) totalInstructions / (double) cycles));
    fprintf(outputFile, "CorrectTaken : %lu\n", bp->getCorrectTaken());
    fprintf(outputFile, "PredictedTaken : %lu\n", bp->getPredictedTaken());
    fprintf(outputFile, "ActualTaken : %lu\n", bp->getActualTaken());
    fprintf(outputFile, "CorrectNotTaken : %lu\n", bp->getCorrectNotTaken());
    fprintf(outputFile, "PredictedNotTaken : %lu\n", bp->getPredictedNotTaken());
    fprintf(outputFile, "ActualNotTaken : %lu\n", bp->getActualNotTaken());
    fprintf(outputFile, "Accuracy : %f\n", ((double) (bp->getCorrectTaken() + bp->getCorrectNotTaken()) /
                                            (double) (bp->getActualTaken() + bp->getActualNotTaken())));
    fprintf(outputFile, "CacheHits : %lu\n", cacheHits);
    fprintf(outputFile, "CacheMisses : %lu\n", cacheMisses);
    fprintf(outputFile, "Accuracy : %f\n", ((double) cacheHits / (double) (cacheHits + cacheMisses)));
    fprintf(outputFile, "TextSize : %u\n", (mem->getDataAddressStart() - mem->getTextAddressStart()) / 4);
    fprintf(outputFile, "DataSize : %u\n", (mem->getStackAddressEnd() - mem->getDataAddressStart()) / 4);
    fprintf(outputFile, "StackSize : %u\n", (mem->getStackAddressStart() - mem->getStackAddressMin()) / 4);
}

/***************************************************************************//**
    Print out the integer register file.

    @param outputFile - The outputfile to print to.
*//****************************************************************************/
void Datapath::outputIRegFile(FILE* outputFile) {
    uint8_t i;
    for( i=0; i < NUM_INT_REGISTERS; i++) {
        fprintf(outputFile, "%s : %08d\n", iReg->getStr((Arch::Reg_t) i).c_str(), iReg->read((Arch::Reg_t) i).s);
    }
}

/***************************************************************************//**
    Print out the floating point register file.

    @param outputFile - The outputfile to print to.
*//****************************************************************************/
void Datapath::outputFRegFile(FILE* outputFile) {
    uint8_t i;
    for( i=0; i < NUM_INT_REGISTERS; i++) {
        fprintf(outputFile, "%s : %08f\n", fReg->getStr((Arch::Reg_t) i).c_str(), fReg->read((Arch::Reg_t) i).f);
    }
}

/***************************************************************************//**
    Output the data memory to file.

    @param outputFile - The outputfile to print to.
*//****************************************************************************/
void Datapath::outputDataMemory(FILE* outputFile) {
    Arch::Address_t i;
    for( i=mem->getDataAddressStart(); i <= mem->getDataAddressEnd(); i = i + 4) {
        fprintf(outputFile, "%08x : %08d\n", i, mem->read(i).data.s);
    }
}

}
