/***************************************************************************//**
    \file BaseInstruction.hpp
    \brief Header for the base instruction class.
*//****************************************************************************/
#ifndef __BASEINSTRUCTION_HPP__
#define __BASEINSTRUCTION_HPP__

#include "Util.hpp"

#include "BranchPredictor.hpp"
#include "ExecutionUnit.hpp"
#include "RegisterFile.hpp"
#include "Memory.hpp"

/** Forward declaration of dependance on datapath. */
namespace Datapath {
	class Datapath;
}

namespace Insts {

/***************************************************************************//**
	The base class representing an insturction in the datapath pipeline.
*//****************************************************************************/
class Inst {
public:
	Inst(Datapath::Datapath* datapath,
		 Arch::Address_t pc,
		 Arch::UnsignedWord_t rawInstruction,
		 Arch::Op_t opcode,
		 Eu::Type_t euType,
		 Eu::Op_t euOp,
		 Mem::Mode_t memMode,
		 Arch::Reg_t wbReg);
	virtual ~Inst(){};
	static Inst* IF(Datapath::Datapath* datapath);
	Inst* ID(void);
	Inst* EX(void);
	Inst* MEM(void);
	Inst* WB(void);
	std::string getStr(void);
	static std::string lookupOpcodeStr(Arch::Op_t opcode);
	/** Does this instruction depend on a given register. */
	bool dependsOnReg(Arch::Reg_t reg) { return (reg != (Arch::Reg_t) 0) && (dependentRegs.count(reg) > 0); }
	/** There is a control change in IF. */
	bool controlChangeInIf(void) { return (getNextPcDefault() != getNextPcIf()); }
	/** There is a control change in ID. */
	bool controlChangeInId(void) { return (getNextPcIf() != getNextPcId()); }
	/** There is a control change in EX. */
	bool controlChangeInEx(void) { return (getNextPcId() != getNextPcEx()); }
	Arch::UnsignedWord_t getPc(void) { return pc; };
	Arch::UnsignedWord_t getNextPc(void) { return nextPc; };
	Bp::Prediction_t getPredictedBranch(void) { return predictedBranch; };
	Bp::Prediction_t getActualBranch(void) { return actualBranch; };
	Arch::UnsignedWord_t getRawInstruction(void) { return rawInstruction; };
	bool isFpInst(void);
	Arch::InstructionType_t getType(void);
	Arch::Op_t getOpcode(void) { return opcode; };
	std::string getOpcodeStr(void) { return lookupOpcodeStr(opcode); };
	int32_t getExCycles(void) { return exCycles; };
	Eu::Type_t getEuType(void) { return euType; };
	Eu::Op_t getEuOp(void) { return euOp; };
	Arch::Word_t getEuOperandA(void) { return euOperandA; };
	Arch::Word_t getEuOperandB(void) { return euOperandB; };
	Eu::Result_t getEuResult(void) { return euResult; };
	int32_t getMemCycles(void) { return memCycles; };
	Mem::Mode_t getMemMode(void) { return memMode; };
	Arch::Address_t getMemAddress(void) { return memAddress; };
	Arch::Word_t getMemWriteData(void) { return memWriteData; };
	Mem::Transaction_t getMemTransaction(void) { return memTransaction; };
	Arch::Reg_t getWbReg(void) { return wbReg; };
	Arch::Word_t getWbData(void) { return wbData; };
protected:
	virtual std::string getInstructionStr(void) { return getOpcodeStr(); }
	Datapath::Datapath* datapath;        /** The datapath in which this instruction is executing. */
	Arch::UnsignedWord_t pc;             /** The PC of the current instruction. */
	Arch::UnsignedWord_t nextPc; 		 /** The PC of the next instruction to fetch. */
	Bp::Prediction_t predictedBranch;    /** The predicted branch prediction value. */
	Bp::Prediction_t actualBranch;       /** The actual branch prediction value. */
	virtual void setActualBranch(void) { actualBranch = Bp::Prediction_None; };
	virtual Arch::Address_t getNextPcDefault(void) { return pc + 4; };
	virtual Arch::Address_t getNextPcIf(void) { return getNextPcDefault(); };
	virtual Arch::Address_t getNextPcId(void) { return getNextPcIf(); };
	virtual Arch::Address_t getNextPcEx(void) { return getNextPcId(); };
	Arch::UnsignedWord_t rawInstruction; /** The raw instruction word. */
	Arch::Op_t opcode; 					 /** The opcode of the instruction */
	std::set<Arch::Reg_t> dependentRegs; /** Registers this instruction depends on. */
	virtual void setDependentRegs(){};
	int32_t exCycles;					 /** The number of cycles it takes for the execution stage to complete. */
	Eu::Type_t euType; 					 /** The execution unit type to use. */
	Eu::Op_t euOp; 						 /** The execution unit operation to perform. */
	Arch::Word_t euOperandA; 			 /** The A operand for the execution unit. */
	Arch::Word_t euOperandB; 			 /** The B operand for the execution unit. */
	virtual void setEuOperands(void);
	Eu::Result_t euResult; 				 /** The result of the execution unit. */
	int32_t memCycles;					 /** The number of cycles it takes for the memory stage to complete. */
	Mem::Mode_t memMode;				 /** The mode to operate memory in. */
	Arch::Address_t memAddress; 		 /** The memory address to access. */
	Arch::Word_t memWriteData; 			 /** The memory value to write if write enabled. */
	virtual void setMemWriteData(void);
	virtual Mem::Transaction_t memSpecialTransaction(void) { return Mem::defaultTransaction; };
	Mem::Transaction_t memTransaction; 	 /* The resulting memory transaction. */
	Arch::Reg_t wbReg; 					 /** The write back register. */
	Arch::Word_t wbData; 				 /** The write back data. */
	virtual void setWbData(void);
};

extern Inst* InstFactoryMethod(Datapath::Datapath* datapath, Arch::Address_t pc, Arch::UnsignedWord_t rawInstruction);

}

#endif /* __BASEINSTRUCTION_HPP__ */
