# project name (generate executable with this name)
TARGET     = neurosim
TEST       = neurosim_test
CC         = g++
# compiling flags here
CFLAGS   = -Wall -I. -std=c++11

LINKER   = g++ -o
# linking flags here
LFLAGS   = -Wall -I. -lm
LDLIBS   = -lm

SRCDIR   = src
TSTDIR   = test
OBJDIR   = obj
BINDIR   = bin

SRCS     := $(wildcard $(SRCDIR)/*.cpp)
TSTS     := $(wildcard $(TSTDIR)/*.cpp)
SRCOBJS  := $(SRCS:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
TSTOBJS  := $(TSTS:$(TSTDIR)/%.cpp=$(OBJDIR)/%.o)
RM       = rm -rf
MKDIR    = mkdir -p

# Build main source
.PHONY: build
build: $(BINDIR)/$(TARGET)
$(BINDIR)/$(TARGET): $(SRCOBJS)
	@$(MKDIR) $(@D)
	@$(LINKER) $@ $(LFLAGS) $(SRCOBJS) $(LDLIBS)
	@echo "Linking complete!"

$(SRCOBJS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@$(MKDIR) $(@D)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

.PHONY: run
run:
	@echo "Running" ./$(BINDIR)/$(TARGET)
	@./$(BINDIR)/$(TARGET) -sri -c samples/config.cfg -f samples/test_spu.bin

# Build test
.PHONY: buildtest
buildtest: $(BINDIR)/$(TEST)
$(BINDIR)/$(TEST): $(TSTOBJS)
	@$(MKDIR) $(@D)
	@$(LINKER) $@ $(LFLAGS) $(TSTOBJS) $(LDLIBS)
	@echo "Linking complete!"

$(TSTOBJS): $(OBJDIR)/%.o : $(TSTDIR)/%.cpp
	@$(MKDIR) $(@D)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

.PHONY: test
test:
	@echo "Running" ./$(BINDIR)/$(TEST)
	@./$(BINDIR)/$(TEST)

# Build main source and test
.PHONY: all
all: build buildtest

# Cleanup targets
.PHONY: clean
clean:
	@$(RM) $(OBJDIR)
	@echo "Cleanup complete!"

.PHONY: rm
rm: clean
	@$(RM) $(BINDIR)
	@echo "Executables removed!"
	@$(RM) statistics.txt
	@$(RM) registers.txt
	@$(RM) trace.txt
	@echo "Simulator output files removed!"
